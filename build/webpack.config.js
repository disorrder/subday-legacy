'use strict';
const os = require('os');
const path = require('path');
const webpack = require('webpack');

__dirname = path.resolve(__dirname, ".."); // process.cwd();
const cfg = {
    "path": {
        "api": "./api/",
        "src": "./src/",
        "build": "./.build/"
    },
    "api": require("../config/api")
};

const WebpackNotifierPlugin = require('webpack-notifier');
const {CleanWebpackPlugin}  = require('clean-webpack-plugin');
const CopyWebpackPlugin   = require('copy-webpack-plugin');
const HtmlWebpackPlugin   = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");


// env variables
process.env.WEBPACK = true;

let mode = process.env.NODE_ENV || "development";
let argvModeIndex = process.argv.findIndex(v => v === "--mode");
if (~argvModeIndex) {
    mode = process.argv[argvModeIndex + 1];
}
console.log("MODE:", mode)

module.exports = {
    context: path.resolve(__dirname, cfg.path.src),
    entry: {
        main: "index.js",
    },
    output: {
        path: path.resolve(__dirname, cfg.path.build),
        publicPath: '/',
        filename: '[name].[hash].js',
        library: '[name]'
    },
    devServer: {
        host: "0.0.0.0",
        public: "localhost:8080",
        open: true,
        hot: true,
        disableHostCheck: true,
        historyApiFallback: true,
        contentBase: [cfg.path.src, "./.public"],
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        },
        stats: 'minimal',
        proxy: {
            "/api": {
                target: `http://0.0.0.0:${cfg.api.port}`,
                pathRewrite: {"^/api/": "/"},
            },
            "/socket.io": {
                ws: true,
                target: `http://0.0.0.0:${cfg.api.port}`,
            },
        }
    },
    resolve: {
        modules: [
            path.join(__dirname, "src"),
            "node_modules",
        ],
        alias: {
            "@": path.join(__dirname, "src"),
            "utils": path.join(__dirname, "utils"),
            vue: 'vue/dist/vue.js'
        }
    },
    module: {
        rules: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
            { test: /\.(pug|jade)$/, loader: "pug-loader", options: {} },
            { test: /\.css$/, use: [ {loader: MiniCssExtractPlugin.loader, options: {hmr: mode === "development"}}, "css-loader" ] },
            { test: /\.styl$/, use: [ {loader: MiniCssExtractPlugin.loader, options: {hmr: mode === "development"}}, "css-loader", "stylus-loader"] },
            { test: /\.glsl|.vert|.frag$/, loader: "webpack-glsl-loader" },
            { // pictures and fonts
                test: /\.(jpeg|jpg|png|gif|woff2?|svg|ttf|eot|fnt)$/i,
                loader: "file-loader",
                options: {
                    name: "[path][name].[ext]",
                    esModule: false,
                }
            },
            { // 3d assets
                test: /\.(gltf|glb|fbx|obj|mtl|dat|patt)$/i,
                loader: "file-loader",
                options: {
                    name: "[path][name].[ext]"
                }
            },
            { // icon fonts
                test: /\.?font\.js/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'webfonts-loader'
                ]
            }
        ],
        noParse: /\.min\.js$/
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
        }
    },
    plugins: [
        new CleanWebpackPlugin(),
        new WebpackNotifierPlugin({excludeWarnings: true}),
        new webpack.HotModuleReplacementPlugin(),

        new webpack.LoaderOptionsPlugin({
            debug: true
        }),

        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.pug',
            inject: 'body',
        }),

        new MiniCssExtractPlugin({
            filename: mode === "development" ? "[name].css" : "[name].[hash].css"
        }),

        new CopyWebpackPlugin([
            // { from: 'config.js' },
            { from: 'favicon.*' },
            // { from: 'robots.txt' },
        ]),

        new webpack.DefinePlugin({
            VERSION: JSON.stringify( require("../package.json").version ),
            REVISION: JSON.stringify( require("child_process").execSync('git rev-parse --short HEAD').toString().trim() ),
            BRANCH: JSON.stringify( require("child_process").execSync('git rev-parse --abbrev-ref HEAD').toString().trim() ),
            BUILD_DATE: JSON.stringify( new Date().toJSON() ),
            // config params from CI
            "process.env.WEB_CONFIG": process.env.WEB_CONFIG,
        }),
    ]
}

const fs = require('fs');
{ // check web app config
    let toPath = path.resolve(__dirname, "src/config.js");
    if (!fs.existsSync(toPath)) {
        fs.writeFileSync(toPath, "window.localConfig = {}");
    }
}
{ // check api app config
    let toPath = path.resolve(__dirname, "api/config.js");
    if (!fs.existsSync(toPath)) {
        fs.writeFileSync(toPath, "module.exports = {}");
    }
}
