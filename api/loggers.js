const path = require('path');
const winston = require('winston');
// const cfg = require('./config');

// check logs folder
{
    const fs = require('fs-extra');
    fs.ensureDir(config.logs.folder);
}

function formatHuman(info, ctx) {
    return [info.timestamp, info.level.toUpperCase(), info.ip, info.username, '--', info.message].filter((v) => v).join(' ');
}

function addCtxParams(ctx, message) {
    if (!message) return ctx;
    if (typeof message === 'string') message = {message};

    message.ip = ctx.ip;
    if (!message.username) message.username = ctx.state.user ? ctx.state.user.username : 'UNAUTHORIZED';
    return message;
}

var logger = winston.createLogger({
    // UI formatted logs
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
    ),

    transports: [
        new winston.transports.Console({
            json: false,
            format: winston.format.printf(formatHuman),
        }),
        new winston.transports.File({ filename: path.join(config.logs.folder, 'error.log'), level: 'error' }),
        new winston.transports.File({ filename: path.join(config.logs.folder, 'stdout.log') }),
        // Human-readable logs
        new winston.transports.File({
            filename: path.join(config.logs.folder, 'stdout-human.log'),
            json: false,
            format: winston.format.printf(formatHuman),
        })
    ]
});

var authLogger = winston.createLogger({
    // format: winston.format.json(),
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
    ),
    transports: [
        new winston.transports.Console({
            json: false,
            format: winston.format.printf(formatHuman),
        }),
        new winston.transports.File({ filename: path.join(config.logs.folder, 'auth.log') }),
        // Human-readable logs
        new winston.transports.File({
            filename: path.join(config.logs.folder, 'auth-human.log'),
            json: false,
            format: winston.format.printf(formatHuman),
        })
    ]
});

module.exports = {
    logger,
    authLogger,

    addCtxParams,
    info(ctx, message) {
        return logger.info(addCtxParams(ctx, message));
    },
    error(ctx, message) {
        return logger.error(addCtxParams(ctx, message));
    },
    auth(ctx, message) {
        return authLogger.info(addCtxParams(ctx, message));
    },
};
