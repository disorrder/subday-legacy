var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const twitchAPI = require("../../external/twitch");

var schema = new Schema({
    email: { type: String, lowercase: true, trim: true, select: false  },
    username: { type: String, lowercase: true, trim: true  },
    // Security
    confirmToken: { type: String, select: false },
    resetToken: { type: String, select: false },
    access: {
        isConfirmed: { type: Boolean, default: false }, // email
        isStreamer: Boolean,
        isAdmin: Boolean,
    },
    // Data
    personal: {
        name: String,
        birthdate: Date,
    },
    premium: {
        tier: { type: Number, default: 0 },
        expiresAt: Date,
    },
    twitch: {
        accessToken: { type: String, select: false },
        refreshToken: { type: String, select: false },
        id: { type: String, index: { unique: true } },
        login: String,
        display_name: String,
        profile_image_url: String,
        email: { type: String, select: false },
        blacklist: [String], // Lifetime free subs
        subscribers: {
            count: Number,
            score: Number,
            lastUpdate: Date,
        },
    },
    team: [
        {
            user: {type: Schema.Types.ObjectId, ref: 'User', require: true},
            role: {type: String, default: "moderator"},
        }
    ],
    // Preferences
    // preferences: {
    //     // emailTypes: Schema.types.Mixed
    // }
    admcomment: { type: String, select: false },
}, { timestamps: true });

schema.methods.verifyPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

schema.methods.safe = function () {
    let safeUser = this.toJSON();
    delete safeUser.premium;
    if (safeUser.twitch) {
        delete safeUser.twitch.subscribers;
    }
    return safeUser;
};

schema.methods.refreshTwitchToken = async function () {
    let data = await twitchAPI.refreshToken(this.twitch.refreshToken);
    this.twitch.accessToken = data.access_token;
    this.twitch.refreshToken = data.refresh_token;
    this.save();
    return data;
};

schema.pre('save', async function() {
    if (!this.isModified('password')) return;
    this.password = bcrypt.hashSync(this.password, 10); // 10 - salt work factor
});

var Model = mongoose.model('User', schema);

module.exports = Model;
