const CrudController = require('../CrudController');
const Model = require('./model');
const sendEmail = require('../../email/send');
const axios = require('axios');
const twitchAPI = require("../../external/twitch");
const logger = require('../../loggers');

class Controller extends CrudController {
    static get name() { return 'User' }
    constructor() {
        super(Model);
        this.protectedFields = [
            ...this.protectedFields,
            "access",
            "twitch",
            "email",
            "username"
        ];
        this.maxLimit = 100500;
    }

    // remapItem(ctx, item) {
    // }

    remapList(ctx, list) {
        if (ctx.state.user.access.isAdmin) return list;
        return list.map(item => {
            if (item._id.equals(ctx.state.user._id)) return ctx.state.user;

            item = item.toJSON();
            if (!item.access) item.access = {};
            if (!item.premium) item.premium = {};
            if (!item.twitch.subscribers) item.twitch.subscribers = {};
            return item;
        });
    }

    async create(ctx) { // Registration
        var data = ctx.request.body;

        if (data.password !== data.password2) {
            return ctx.throw(400, 'ERR_PASSWORDS_MISMATCH');
        }
    
        var user = new Model(data);
        user.confirmToken = String.randomize(16);
    
        if (await Model.findOne({username: data.username})) {
            return ctx.throw(400, 'ERR_USERNAME_BUSY');
        }
        if (await Model.findOne({email: data.email})) {
            return ctx.throw(400, 'ERR_EMAIL_BUSY');
        }
    
        try {
            await sendEmail.confirmEmail({to: user.email, confirmToken: user.confirmToken, origin: ctx.get('origin')});
        } catch (e) {
            // данная ошибка может возникнуть если, например, email имеет вид test@testdevfortest.nonexistcertainly
            console.log('EMAIL ERROR: ' + e);
            return ctx.throw(400, 'ERR_EMAIL_INCORRECT');
        }
    
        try {
            await user.save();
            await ctx.login(user);
        } catch(e) {
            console.log('ERROR message: ' + e.message);
            console.log('500', '/user/add', e);
            return ctx.throw(500);
        }
        ctx.body = user;    
    }

    async getMyUser(ctx) { // ref getMe
        let user = await Model.findById(ctx.state.user._id).select("+twitch.accessToken +twitch.email");
        ctx.body = user;
    }

    async getById(ctx) {
        if (!ctx.state.user.access.isAdmin
            && ctx.state.user._id !== ctx.params.id
        ) {
            ctx.state.select = "-access -twitch.subscribers -premium";
        }
        return super.getById(ctx);
    }

    async getList(ctx) {
        if (!ctx.state.user.access.isAdmin) {
            ctx.state.select = "-access -twitch.subscribers -premium";
        }
        return super.getList(ctx);
    }

    async check(ctx) {
        var query = [];
        if (ctx.query.username) query.push({username: ctx.query.username});
        if (ctx.query.email) query.push({email: ctx.query.email});
        var user = await Model.findOne({$or: query});
        if (!user) return ctx.body = false;
        if (user.username === ctx.query.username) return ctx.body = "USERNAME_BUSY";
        if (user.email === ctx.query.email) return ctx.body = "EMAIL_BUSY";
        ctx.body = false; //?
    }

    async update(ctx) {
        let user = await this.findById(ctx);
        let body = ctx.request.body;

        if (ctx.state.user.access.isAdmin) {
            user.access = body.access;
            user.admcomment = body.admcomment;
        }

        user.personal = body.personal;
        user.twitch.blacklist = body.twitch.blacklist;
        await user.save();
        
        ctx.body = user;
        return user;
    }

    async updatePassword(ctx) {
        let data = ctx.request.body;

        let user = await Model.findById(ctx.state.user.id).select('+password');
        if (!user) return ctx.throw(403, 'ERR_INCORRECT_USERNAME');
        if (!user.verifyPassword(data.oldPassword)) return ctx.throw(403, 'ERR_INCORRECT_PASSWORD');
    
        user.password = data.newPassword;
        await user.save();
    
        return ctx.body = 'ok';
    }

    async requestStreamer(ctx) {
        ctx.state.user.access.isStreamer = true;
        await ctx.state.user.save();
        return ctx.body = 'ok';
    }

    // Twitch
    async checkTwitchFollow(ctx) {
        let accessToken = ctx.state.user.twitch.accessToken;
        let params = {
            from_id: ctx.state.user.twitch.id,
            to_id: ctx.params.owner_twitch_id
        };
        try {
            let {data} = await twitchAPI.getFollows(accessToken, params);
            ctx.body = data;
        } catch(e) {
            logger.error(ctx, `Follow check error: ${e.response.data.message}`);
            ctx.throw(e.response.data.status, e.response.data);
        }
    }

    async checkTwitchSubscription(ctx) {
        // К сожалению, твич в новой версии апи (helix) не имеет метода для проверки подписки юзера на канал,
        // Приходится проверять токеном канала
        let owner = await Model.findById(ctx.params.owner_id).select("+twitch.accessToken +twitch.refreshToken");
        if (!owner) return ctx.throw(404);

        async function checkSub() {
            let accessToken = owner.twitch.accessToken;
            let params = {
                broadcaster_id: owner.twitch.id,
                user_id: ctx.state.user.twitch.id,
            };

            let {data} = await twitchAPI.checkSubscription(accessToken, params);
            ctx.body = data;
            return data;
        }

        try { // ref like getTwitchSubs
            await checkSub();
        } catch(e) {
            if (e.response && e.response.data.status === 401) {
                await this.twitchRefreshToken(owner);
                return checkSub();
            }
            logger.error(ctx, `Subscription check error: ${e.response.data.message}`);
            ctx.throw(e.response.data.status, e.response.data);
        }
    }

    async getTwitchSubs(ctx) { //? mb delete? Count do the same
        let user = ctx.state.user;
        if (user.access.isAdmin) {
            if (ctx.query.user_id) {
                user = await Model.findById(ctx.query.user_id).select("+twitch.accessToken +twitch.refreshToken");
            }
        }
        let accessToken = user.twitch.accessToken;
        let params = {
            broadcaster_id: user.twitch.id,
        };

        async function request() {
            return twitchAPI.getAllSubscribers(accessToken, params);
        }

        let data = await request().catch(async (e) => {
            if (e.response && e.response.data.error === "Unauthorized") {
                await user.refreshTwitchToken();
                accessToken = user.twitch.accessToken;
                return request();
            }
            ctx.throw = e;
        });

        user.twitch.subscribers.count = data.length;
        // user.twitch.subscribers.score = data.score;
        user.twitch.subscribers.lastUpdate = new Date();
        user.save();

        ctx.body = data;
    }

    async getTwitchSubsCount(ctx) {
        let user = ctx.state.user;
        if (ctx.query.user_id) { // user.access.isAdmin
            user = await Model.findById(ctx.query.user_id).select("+twitch.accessToken +twitch.refreshToken");
        }
        let accessToken = user.twitch.accessToken;
        let params = {
            broadcaster_id: user.twitch.id,
        };

        async function request() {
            return twitchAPI.getAllSubscribers(accessToken, params);
        }

        let subs = await request().catch(async (e) => {            
            if (e.response && e.response.data.error === "Unauthorized") {
                await user.refreshTwitchToken();
                accessToken = user.twitch.accessToken;
                return request();
            }
            ctx.throw = e;
        });

        let points = {
            1000: 1,
            2000: 2,
            3000: 6
        };
        let data = {
            count: 0, score: 0, gifts: 0,
            count_breakdown: {1000: 0, 2000: 0, 3000: 0},
            score_breakdown: {1000: 0, 2000: 0, 3000: 0},
        };
        let blacklist = user.twitch.blacklist.map(v => v.toLowerCase());
        subs.forEach(sub => {
            if (blacklist.includes(sub.user_name.toLowerCase())) return;
            data.count++;
            data.score += points[sub.tier];
            data.count_breakdown[sub.tier]++;
            data.score_breakdown[sub.tier] += points[sub.tier];
            if (sub.is_gift) data.gifts++;
        });
        // data.subs = subs; // Debug

        user.twitch.subscribers.count = data.count;
        user.twitch.subscribers.score = data.score;
        user.twitch.subscribers.lastUpdate = new Date();
        user.save();

        ctx.body = data;
    }

    // Twitch API
    async twitchRefreshToken(user) {
        let data = await twitchAPI.refreshToken(user.twitch.refreshToken);
        user.twitch.accessToken = data.access_token;
        user.twitch.refreshToken = data.refresh_token;
        await user.save();
        return user;
    }

    // Middlewares
    async checkOwner(ctx, next) {
        var item = ctx.item || await this.findById(ctx);
        if (!ctx.state.user.access.isAdmin || !ctx.state.user._id.equals(item._id)) return ctx.throw(403, 'ERR_OWNER_MISMATCH');
        ctx.state.isOwner = true;
        if (next) return next();
    }
}

module.exports = new Controller();