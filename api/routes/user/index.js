const ctrl = require('./controller');
const Router = require('koa-router');
var router = new Router();

// Registration
router.post('/add', ctrl.create.bind(ctrl));
router.get('s', ctrl.checkAuth.bind(ctrl), ctrl.getList.bind(ctrl));

router.get('/check', ctrl.check.bind(ctrl));
// router.put('/password', ctrl.updatePassword.bind(ctrl));

router.get('/me', ctrl.checkAuth.bind(ctrl), ctrl.getMyUser.bind(ctrl));
router.get('/request-streamer', ctrl.checkAuth.bind(ctrl), ctrl.requestStreamer.bind(ctrl));

router.get('/:id', ctrl.checkAuth.bind(ctrl), ctrl.getById.bind(ctrl));
router.patch('/:id', ctrl.checkOwner.bind(ctrl), ctrl.update.bind(ctrl));
// router.delete('/:id', ctrl.delete.bind(ctrl));


router.get('/twitch/check-follow/:owner_twitch_id', ctrl.checkAuth.bind(ctrl), ctrl.checkTwitchFollow.bind(ctrl));
router.get('/twitch/check-sub/:owner_id', ctrl.checkAuth.bind(ctrl), ctrl.checkTwitchSubscription.bind(ctrl));
router.get('/twitch/get-subs-count', ctrl.getTwitchSubsCount.bind(ctrl));
// router.get('/twitch/get-subs', ctrl.checkAuth.bind(ctrl), ctrl.getTwitchSubs.bind(ctrl));

module.exports = router;