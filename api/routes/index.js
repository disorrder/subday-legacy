const Router = require('koa-router');
const router = new Router();

{
    let route;
    route = require('./auth');
    router.use('/auth', route.routes(), route.allowedMethods());

    route = require('./user');
    router.use('/user', route.routes(), route.allowedMethods());

    route = require('./poll');
    router.use('/poll', route.routes(), route.allowedMethods());

    route = require('./widget-type');
    router.use('/widget-type', route.routes(), route.allowedMethods());

    route = require('./widget');
    router.use('/widget', route.routes(), route.allowedMethods());

    route = require('./twitch');
    router.use('/twitch', route.routes(), route.allowedMethods());

    route = require('./steam-api');
    router.use('/steam-api', route.routes(), route.allowedMethods());
}

router.get('/', (ctx, next) => {
    // ctx.router available
    ctx.body = 'Hi there! This is my API. Feel free and good luck!';
});

const request = require("request");
router.get("/proxy", async (ctx) => {
    ctx.body = request(ctx.query.url);
});

module.exports = router;
