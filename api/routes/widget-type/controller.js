const CrudController = require('../CrudController');
const Model = require('./model');

class Controller extends CrudController {
    static get name() { return 'WidgetType' }
    constructor() {
        super(Model);
    }

}

module.exports = new Controller();
// module.exports.class = Controller;
