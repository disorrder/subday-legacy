const ctrl = require('./controller');
const Router = require('koa-router');
var router = new Router();

router.get('s', ctrl.getList.bind(ctrl));

router.post('/', ctrl.checkAdmin.bind(ctrl), ctrl.create.bind(ctrl));
router.get('/:id', ctrl.getById.bind(ctrl));
router.patch('/:id', ctrl.checkAdmin.bind(ctrl), ctrl.update.bind(ctrl));
router.delete('/:id', ctrl.checkAdmin.bind(ctrl), ctrl.delete.bind(ctrl));

module.exports = router;
