var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    disabled: Boolean,
    name: String, // тех. поле, человеческое id типа виджета. Например, "roulette"
    title: String,
    description: String,
    icon: String,
    color: String,
    iconStyle: String,
    actions: [],
    responsive: Boolean, // если true, виджет растягивается по ширине экрана
    fullScreen: Boolean, // если true, виджет растягивается на весь экран (100vw, 100vh)
    owner: {type: Schema.Types.ObjectId, ref: 'User', require: !true},
}, { timestamps: true });

var Model = mongoose.model('WidgetType', schema);

module.exports = Model;
