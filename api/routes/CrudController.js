// const {logger, addCtxParams} = require("./loggers");
const logger = require("../loggers");

module.exports = class CrudController {
    static get name() { return "CRUD" }
    constructor(Model) {
        this.Model = Model;
        // protectedFields используются для защиты от записи в методе update.
        // Чтобы перезаписать поля из этого списка, необходимо создать дополнительный метод
        this.protectedFields = ["_id", "createdAt", "updatedAt", "createdBy", "owner", "publishedAt"];
        this.maxLimit = 100;
    }

    async create(ctx) {
        var data = ctx.request.body;
        var item = new this.Model(data);
        try {
            item.owner = ctx.state.user._id; // Используется для приватности. Обычно если owner установлен, то документ не публичный
            await item.save();
        } catch(e) {
            console.error(e.message);
            if (e.code === 11000) return ctx.throw(400, "ERR_DUPLICATE");
            if (e.code) return ctx.throw(400, e.message);
            return ctx.throw(500, e.message);
        }

        ctx.body = item;
        logger.info(ctx, `Create ${this.name} ${this.constructor.name}. _id: ${item._id}.`);
        return item;
    }

    // async remapItem(ctx, item) {
    //     return item;
    // }
    // async remapList(ctx, list) {
    //     return list;
    // }

    // chunks
    async findById(ctx) {
        let req = this.Model.findById(ctx.params.id);
        if (ctx.state.select) req.select(ctx.state.select);

        var item = await req;        
        if (!item) return ctx.throw(404);        
        return ctx.state.item = item;
    }

    paginate(ctx, query) {
        let {sort, skip, limit} = ctx.query;
        limit = Math.min(limit, this.maxLimit) || this.maxLimit;
        if (sort) query.sort(sort)
        if (skip) query.skip(skip)
        query.limit(limit);
    }
    
    // query methods are kind of private promises. Redefine this if you want to modify query to DB
    async getById(ctx) {
        var item = ctx.state.item || await this.findById(ctx);
        if (this.remapItem) item = await this.remapItem(ctx, item);
        ctx.body = item;
        return item;
    }

    async getList(ctx) {
        let query = {};
        if (ctx.query["ids[]"]) query._id = ctx.query["ids[]"];
        Object.assign(query, ctx.state.query);
        
        let req = this.Model.find(query);
        this.paginate(ctx, req);
        if (ctx.state.select) req.select(ctx.state.select);
        
        var list = await req;
        if (this.remapList) list = await this.remapList(ctx, list);
        ctx.body = list;
        return list;
    }

    async _updateQuery(ctx, data) { //?
        // тут pre save не сработает, зато можно писать напрямую в базу, даже без валидации (это вообще здесь нужно?)
        if (!data) data = ctx.request.body;
        this.protectedFields.forEach((v) => delete data[v]);
        try {
            var item = await this.Model.findByIdAndUpdate(ctx.params.id, data, {new: true});
        } catch(e) {
            console.error(`[${this.constructor.name}] _updateQuery:`, e, e.message);
            ctx.throw(500, e.message);
        }
        if (!item) return ctx.throw(404);
        return item;
    }

    async update(ctx) {
        // Вместо findByIdAndUpdate используется save для того, чтобы сработал хук pre save
        var item = ctx.state.item || await this.findById(ctx);
        let data = ctx.request.body;
        this.protectedFields.forEach((v) => delete data[v]);
        item.set(data);
        
        try {
            item = await item.save();
        } catch(e) {
            console.error(`[${this.constructor.name}] update:`, e, e.message);
            ctx.throw(500, e.message);
        }

        if (this.remapItem) item = await this.remapItem(ctx, item);
        ctx.body = item;
        logger.info(ctx, `Update ${this.constructor.name}. _id: ${item._id}.`);
        return item;
    }

    async delete(ctx) {
        try {
            var item = await this.Model.findByIdAndDelete(ctx.params.id);
        } catch(e) {
            console.error(e, e.message);
            ctx.throw(500, e.message);
        }
        
        ctx.body = "OK";
        logger.info(ctx, `Delete ${this.constructor.name}. _id: ${item._id}.`);
        return item;
    }
    
    async disable(ctx) { //?
        let item = await this._updateQuery(ctx, {__deleted: true});
        ctx.body = "OK";
        logger.info(ctx, `Disable ${this.constructor.name}. _id: ${item._id}.`);
        return item;
    }
    async enable(ctx) { //?
        let item = await this._updateQuery(ctx, {__deleted: null});
        ctx.body = "OK";
        logger.info(ctx, `Enable ${this.constructor.name}. _id: ${item._id}.`);
        return item;
    }

    // middlewares
    checkAuth(ctx, next) {
        if (!ctx.isAuthenticated()) return ctx.throw(401);
        if (next) return next();
    }

    async checkOwner(ctx, next) {
        var item = ctx.state.item || await this.findById(ctx);
        let allow = false;
        if (ctx.state.user.access.isAdmin) allow = true;
        if (ctx.state.user._id.equals(item.owner)) allow = true;
        if (!allow) return ctx.throw(403, "ERR_OWNER_MISMATCH");
        if (next) return next();
    }

    checkAccess(...fields) {
        return function checkAccessDecorator(ctx, next) {
            this.checkAuth(ctx);
            fields.forEach((field) => {
                if (!ctx.state.user.access[field]) return ctx.throw(403);
            });
            if (ctx.status === 403) return;
            if (next) return next();
        }
    }

    checkConfirmed(ctx, next) {
        this.checkAuth(ctx);
        if (!ctx.state.user.access.isConfirmed) return ctx.throw(403);
        if (next) return next();
    }

    checkTester(ctx, next) {
        this.checkAuth(ctx);
        if (!ctx.state.user.access.isTester) return ctx.throw(403);
        if (next) return next();
    }

    checkAdmin(ctx, next) {
        this.checkAuth(ctx);
        if (!ctx.state.user.access.isAdmin) return ctx.throw(403);
        if (next) return next();
    }

    checkDev(ctx, next) {
        this.checkAuth(ctx);
        if (!ctx.state.user.access.isDeveloper) return ctx.throw(403);
        if (next) return next();
    }
}
