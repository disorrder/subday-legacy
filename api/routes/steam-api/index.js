const Router = require('koa-router');
var router = new Router();

const axios = require("axios");

router.get('/games', async (ctx) => {
    let {data} = await axios.get("http://api.steampowered.com/ISteamApps/GetAppList/v2");
    data = data.applist.apps;
    let dlcReg = /\s(DLC|pack|-\s)/i;
    data = data.filter((v) => {
        if (dlcReg.test(v.name)) return false;
        return true;
    });
    ctx.body = data;
});


module.exports = router;
