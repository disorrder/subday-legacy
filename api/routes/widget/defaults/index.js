module.exports.roulette = {
    "title": "Пример рулетки",
    "styles": {
        "borderWidth": 5
    },
    position: {x: "start", y: "start"},
    "settings": {
        "rarityTable": [
            {
                "color": "blue-grey lighten-2",
                "value": 45
            },
            {
                "color": "green",
                "value": 25
            },
            {
                "color": "light-blue",
                "value": 15
            },
            {
                "color": "purple",
                "value": 10
            },
            {
                "color": "orange",
                "value": 5
            }
        ],
    
        "options": [
            {
                "title": "Common",
                "rarity": 1,
                "image": "https://i.pinimg.com/originals/1f/ff/94/1fff943c932f7774bdf3d3b87ca923ca.jpg"
            },
            {
                "title": "Uncommon",
                "rarity": 2,
                "image": "https://pbs.twimg.com/media/CUpgIf9XAAEBmLv.jpg"
            },
            {
                "title": "Rare",
                "rarity": 3,
                "image": "https://f.vividscreen.info/soft/1bc8a3364fa96c235c90b3871eea2811/Kung-Fu-Panda-3-2880x1920.jpg"
            },
            {
                "title": "Epic",
                "rarity": 4,
                "image": "http://mediasat.info/wp-content/uploads/2017/10/kung-fu-panda.jpg"
            },
            {
                "title": "Legendary",
                "rarity": 5,
                "image": "https://thumbs.gfycat.com/FrighteningScentedAgouti-mobile.jpg"
            },
        ],
    },
};


module.exports.timer = {
    title: "На полчаса",
    styles: {
        background: "rgba(0, 0, 0, 0.5)",
        color: "#fff",
    },
    position: {x: "center", y: "center"},
    settings: {
        beganAt: null,
        amount: 30*60*1000,
        // showSec: false,
        showMs: false,
        text: "Какая-то надпись",
    }
};


module.exports.snow = {
    title: "Метелица",
    styles: {
        color: "rgba(86, 144, 226, 0.7)",
        blur: 0,
    },
    position: {x: "center", y: "center"},
    settings: {
        count: 100,
        velocity: [2, 1],
        velocityThreshold: [1, 1],
        parallax: false,
    }
};


module.exports.subscore = {
    title: "Сабпоинты",
    styles: {
        color: "rgba(86, 144, 226, 0.7)",
        fontFamily: "Roboto",
        fontSize: 30,
    },
    position: {x: "start", y: "start"},
    settings: {
        text: "SUB SCORE: $count ($score)",
    }
};


module.exports["sub-particles"] = {
    title: "Sub Particles",
    styles: {
        color: "rgba(86, 144, 226, 0.7)",
        fontFamily: "Roboto",
        fontSize: 30,
    },
    position: {x: "center", y: "center"},
    settings: {

    }
};
