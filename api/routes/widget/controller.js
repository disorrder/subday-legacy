const CrudController = require('../CrudController');
const Model = require('./model');
const WidgetTypeModel = require('../widget-type/model');
const defaultItems = require("./defaults");

class Controller extends CrudController {
    static get name() { return 'Widget' }
    constructor() {
        super(Model);
    }

    async create(ctx) {
        if (!ctx.request.body.type) return ctx.throw(400, "ERR_TYPE_REQUIRED");
        let type = await WidgetTypeModel.findById(ctx.request.body.type);
        let def = defaultItems[type.name];
        Object.assign(ctx.request.body, def);
        return super.create(ctx);
    }

    async getList(ctx) {
        if (!ctx.state.user.access.isAdmin) {
            ctx.state.query = { owner: ctx.state.user._id };
        }
        return super.getList(ctx);
    }
}

module.exports = new Controller();
// module.exports.class = Controller;
