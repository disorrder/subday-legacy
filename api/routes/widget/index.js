const ctrl = require('./controller');
const Router = require('koa-router');
var router = new Router();
const Model = require('./model');
const User = require("../user/model");
const twitchCtrl = require("../twitch/controller");

router.get('s', ctrl.checkAuth.bind(ctrl), ctrl.getList.bind(ctrl));

router.post('/', ctrl.checkAuth.bind(ctrl), ctrl.create.bind(ctrl));
router.get('/:id', ctrl.getById.bind(ctrl));
router.patch('/:id', ctrl.checkOwner.bind(ctrl), ctrl.update.bind(ctrl));
router.delete('/:id', ctrl.checkOwner.bind(ctrl), ctrl.delete.bind(ctrl));

module.exports = router;

io.on("connection", (socket) => {
    socket.on("widget.register", async function(id) {
        socket.join(`widget:${id}`);

        let widget = await Model.findById(id);
        socket.join(`user:${widget.owner}`);
        let owner = await User.findById(widget.owner).select("+twitch.accessToken");
        socket.join(`user:twitch-${owner.twitch.id}`);

        twitchCtrl.subscribe("users/follows", {to_id: owner.twitch.id});
        twitchCtrl.subscribe("subscriptions/events", {broadcaster_id: owner.twitch.id}, owner.twitch.accessToken);
    });

    socket.on("widget.action", async function(msg) {
        let {widgetId, method, data} = msg;
        io.to(`widget:${widgetId}`).emit(`widget.action.${method}`, data);
    });
});
