var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    type: {type: Schema.Types.ObjectId, ref: 'WidgetType', require: true},
    title: String,
    description: String,
    position: {
        x: String, // enum start center end
        y: String,
    },
    animation: {},
    styles: {},
    settings: {},
    owner: {type: Schema.Types.ObjectId, ref: 'User', require: true},
}, { timestamps: true });

var Model = mongoose.model('Widget', schema);

module.exports = Model;
