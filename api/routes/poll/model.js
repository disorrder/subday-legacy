var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    title: String,
    description: String,
    additive: Boolean,
    multiple: Boolean,
    selectMax: Number, // only works with multiple
    hideResults: Boolean,
    participants: {
        followers: {type: Boolean, default: false},
        subscribers: {type: Boolean, default: true},
    },
    type: String, // enum of default, games
    options: [{
        title: String,
    }],
    votes: [{
        user: {type: Schema.Types.ObjectId, ref: 'User', require: true},
        option: {type: Schema.Types.ObjectId},
        timestamp: Date,
    }],
    // options: {
    //     select: false,
    //     type: [{
    //         title: String,
    //     }],
    // },
    // votes: {
    //     select: false,
    //     type: [{
    //         user: {type: Schema.Types.ObjectId, ref: 'User', require: true},
    //         option: {type: Schema.Types.ObjectId},
    //         timestamp: Date,
    //     }],
    // },
    owner: {type: Schema.Types.ObjectId, ref: 'User', require: true},
    publishedAt: Date,
    finishAt: Date,
}, {
    timestamps: true,
    toJSON: {virtuals: true},
});

schema.virtual('votesCount').get(function() {
    return this.votes.length;
});

var Model = mongoose.model('Poll', schema);

module.exports = Model;
