const CrudController = require('../CrudController');
const Model = require('./model');
const User = require('../user/model');
const axios = require('axios');
const twitchApi = require("../../external/twitch");

class Controller extends CrudController {
    static get name() { return 'Poll' }
    constructor() {
        super(Model);
        this.protectedFields = [
            ...this.protectedFields,
            "votes", "publishedAt", "finishAt"
        ];
    }

    remapItem(ctx, item) {
        if (item.hideResults) {
            if (!item.owner.equals(ctx.state.user._id)
                && !ctx.state.user.access.isAdmin
                // TODO add check team
            ) {
                item = item.toJSON();
                item.votes = item.votes.filter(v => v.user.equals(ctx.state.user._id));
                let userVote = item.votes[0];
                if (!userVote) {
                    item.options = [];
                } else {
                    item.options = item.options.filter(option => userVote.option.equals(option._id));
                }
            }
        }
        
        return item;
    }
    
    remapList(ctx, list) {
        return list.map(item => {
            item = item.toJSON();
            delete item.options;
            delete item.votes;
            return item;
        });
    }

    async getById(ctx) {
        ctx.state.select = "+options";
        return await super.getById(ctx);
    }

    async getListMy(ctx) {
        var query = this.Model.find({ owner: ctx.state.user._id }).select("+votes");
        this.paginate(ctx, query);
        let list = await query;
        list = this.remapList(ctx, list);
        ctx.body = list;
    }

    async getListSubscriber(ctx) {
        // TODO: add subscriber users, filter published not finished
        let owners = [];
        var query = this.Model.find({ owner: owners.map(v => v._id) }).select("+votes");
        this.paginate(ctx, query);
        let list = await query;
        list = this.remapList(ctx, list);
        ctx.body = list;
    }

    async publish(ctx) {
        await this.checkOwner(ctx);
        let item = ctx.state.item;
        if (item.publishedAt) {
            ctx.throw(400, "ERR_ALREADY_PUBLISHED");
            return;
        }
        if (item.finishAt < Date.now()) {
            ctx.throw(400, "ERR_ALREADY_FINISHED");
            return;
        }
        item.publishedAt = Date.now();
        await item.save();
        ctx.body = item;
    }

    async unpublish(ctx) {
        await this.checkOwner(ctx);
        let item = ctx.state.item;
        if (!item.publishedAt) {
            ctx.throw(400, "ERR_ALREADY_UNPUBLISHED");
            return;
        }
        item.publishedAt = null;
        await item.save();
        ctx.body = item;
    }

    async finish(ctx) {
        await this.checkOwner(ctx);
        let item = ctx.state.item;
        if (!item.publishedAt) {
            ctx.throw(400, "ERR_NOT_PUBLISHED");
            return;
        }
        if (item.finishAt < Date.now()) {
            ctx.throw(400, "ERR_ALREADY_FINISHED");
            return;
        }
        item.finishAt = Date.now();
        await item.save();
        ctx.body = item;
    }

    addOption(item, option) {
        return option;
    }

    async vote(ctx) {
        let item = await this.findById(ctx);
        let data = ctx.request.body;
        let vote = item.votes.find(v => v.user.equals(ctx.state.user._id));
        if (vote) return ctx.throw(400, "ERR_ALREADY_VOTED");
        if (!data) return ctx.throw(400, "ERR_POLL_OPTION_IS_EMPTY");
        let option = item.options.find(v => v._id.equals(data._id));
        if (!option) option = item.options.find(v => v.title === data.title);
        if (!option) {
            if (!item.additive) {
                return ctx.throw(400, "ERR_POLL_OPTION_IS_INVALID");
            } else {
                item.options.push(data);
                option = item.options[item.options.length - 1];
            }
        }
        
        item.votes.push({
            user: ctx.state.user._id,
            option: option._id,
            timestamp: Date.now(),
        });
        await item.save();
        ctx.body = item;
    }

    async setOwner(ctx) {
        let item = await this.findById(ctx);
        let isAdmin = await this.checkAdmin(ctx);
        let newOwner = await User.findById(ctx.request.body.id);
        if (!newOwner) return ctx.throw(400, "ERR_INVALID_OWNER_ID");
        item.owner = newOwner._id;
        await item.save();
        
        ctx.body = item;
    }

    async getVotes(ctx) {
        let item = await this.Model.findById(ctx.params.id).select("+options +votes").populate("votes.user");

        if (item.hideResults) {
            console.log("votes hide res");
            if (!ctx.state.user._id.equals(item.owner)
                && !ctx.state.user.access.isAdmin
            ) {                
                return ctx.throw(403, "ERR_OWNER_MISMATCH");
            }
        }

        let owner = await User.findById(item.owner).select("+twitch.accessToken +twitch.refreshToken");
        let subs = [];
        for (let i = 0, len = item.votes.length/100; i < len; i++) {
            let ids = item.votes.slice(i*100, (i+1)*100).map(v => v.user.twitch.id);
            let subData;
            try {
                let res = await twitchApi.checkSubscription(owner.twitch.accessToken, {broadcaster_id: owner.twitch.id, user_id: ids});
                subData = res.data;
            } catch (error) {
                console.log(error)
            }

            subs.push(...subData);
        }
        
        let votes = item.votes.map((vote) => {
            vote = vote.toJSON();            
            let sub = subs.find(v => v.user_id === vote.user.twitch.id);
            if (sub) {
                vote.subscribe = {tier: sub.tier};
            }
            return vote;
        })
        ctx.body = votes;
    }
}

module.exports = new Controller();