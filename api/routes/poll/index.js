const ctrl = require('./controller');
const Router = require('koa-router');
var router = new Router();

router.use(ctrl.checkAuth.bind(ctrl));

router.get('s', ctrl.checkAdmin.bind(ctrl), ctrl.getList.bind(ctrl));
router.get('s/my', ctrl.getListMy.bind(ctrl));
// router.get('s/follower', ctrl.getListFollower.bind(ctrl));
router.get('s/subscriber', ctrl.getListSubscriber.bind(ctrl));

router.post('/', ctrl.create.bind(ctrl));
router.get('/:id', ctrl.getById.bind(ctrl));
router.patch('/:id', ctrl.update.bind(ctrl));
router.delete('/:id', ctrl.delete.bind(ctrl));

router.post('/:id/publish', ctrl.publish.bind(ctrl));
router.post('/:id/unpublish', ctrl.unpublish.bind(ctrl));
router.post('/:id/finish', ctrl.finish.bind(ctrl));
router.post('/:id/vote', ctrl.vote.bind(ctrl));
router.get('/:id/votes', ctrl.getVotes.bind(ctrl));

router.put('/:id/owner', ctrl.setOwner.bind(ctrl));

module.exports = router;
