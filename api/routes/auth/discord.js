const User = require('../user/model');

var Router = require('koa-router');
var router = new Router();

const passport = require('koa-passport');
var OAuth2Strategy = require('passport-oauth2').Strategy;
// const sendEmail = require('../email/send'); //?

const middlewares = require('../../middlewares')
const logger = require('../../loggers');

const axios = require('axios');
const discordApi = axios.create({
    baseURL: "https://discordapp.com/api",
    headers: {"Authorization": "Bearer "},
});

passport.use(new OAuth2Strategy({
        authorizationURL: 'https://discordapp.com/api/oauth2/authorize',
        tokenURL: 'https://discordapp.com/api/oauth2/token',
        clientID: config.discord.clientID,
        clientSecret: config.discord.clientSecret,
        callbackURL: "/api/auth/discord/callback",
        scope: ["identify", "email", "guilds"],
        // permissions: 68672,
    },
    async function(accessToken, refreshToken, profile, cb) {
        let tokenType = "Bearer";
        let dConfig = {
            headers: {"Authorization": `${tokenType} ${accessToken}`}
        }
        let dUser = await discordApi.get("/users/@me", dConfig).then(({data}) => data);
        console.log("D_User", accessToken, dUser);
        
        // let dUser = dUserRes.data;
        
        let user = await User.findOne({ "discord.id": dUser.id });
        if (!user) {
            user = new User({
                username: dUser.username,
                email: dUser.email,
                access: {
                    isConfirmed: false, //dUser.verified,
                },
            });
        }

        user.discord = {
            ...user.discord,
            ...dUser,
            tokenType,
            accessToken,
            refreshToken,
        };

        user.save();
        cb(null, user);
    }
));

router.get("/login", middlewares.rateLimit('low'), passport.authenticate("oauth2"));
router.get("/callback", passport.authenticate("oauth2"), async (ctx) => {
    ctx.redirect("/profile");
});

module.exports = router;
