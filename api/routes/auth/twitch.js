const User = require('../user/model');

var Router = require('koa-router');
var router = new Router();

const passport = require('koa-passport');
var OAuth2Strategy = require('passport-oauth2').Strategy;
// const sendEmail = require('../email/send'); //?

const middlewares = require('../../middlewares')
const logger = require('../../loggers');

const axios = require('axios');
const serviceApi = axios.create({
    baseURL: config.twitch.url,
    headers: {"Authorization": "Bearer "},
});

passport.use(new OAuth2Strategy({
        authorizationURL: 'https://id.twitch.tv/oauth2/authorize',
        tokenURL: 'https://id.twitch.tv/oauth2/token',
        clientID: config.twitch.clientID,
        clientSecret: config.twitch.clientSecret,
        callbackURL: "/api/auth/twitch/callback",
        scope: ["user:read:email", "user_subscriptions", "channel:read:subscriptions"],
    },
    async function(accessToken, refreshToken, profile, cb) {
        let tokenType = "Bearer";
        let options = {
            headers: {"Authorization": `${tokenType} ${accessToken}`}
        }

        let res = await serviceApi.get("/users", options).then(({data}) => data);
        let sUser = res.data[0];

        let user = await User.findOne({"twitch.id": sUser.id});
        if (!user) {
            user = new User({
                // username: null,
                // email: null,
                access: {
                    isConfirmed: false,
                },
            });
        }        

        user.twitch = {
            ...user.twitch,
            ...sUser,
            tokenType,
            accessToken,
            refreshToken,
        };

        user.save();
        cb(null, user);
    }
));

router.get("/login", middlewares.rateLimit('low'), passport.authenticate("oauth2"));
router.get("/callback", passport.authenticate("oauth2"), async (ctx) => {
    ctx.redirect("/profile");
});

module.exports = router;
