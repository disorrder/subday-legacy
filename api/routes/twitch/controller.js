// const sendEmail = require('../../email/send');
const axios = require('axios');
const twitchAPI = require("../../external/twitch");
const logger = require('../../loggers');
const User = require("../user/model");
const {sleep, DAY} = require("../../../utils");

// TODO: mb перенести всё в twitchApi???

class Controller {
    static get name() { return 'Twitch' }
    constructor() {
        this.callback = "https://subday.disordered.ru/api/twitch/hub/cb";
        this.hubUrl = "https://api.twitch.tv/helix/webhooks/hub";
        this.hubLease = 864000;
        this.topics = [];
        this.messages = {};

        this.auth();
    }

    async _test() {
        // this.unsubscribeAll();

        // let user = await User.findOne({"twitch.login": "juice"}).select("+twitch.accessToken");
        // let userId = user.twitch.id;
        // this.subscribe("users/follows", {to_id: userId});
        // this.subscribe("subscriptions/events", {broadcaster_id: userId}, user.twitch.accessToken);
        // console.log("check sub", await twitchAPI.checkSubscription(user.twitch.accessToken, {broadcaster_id: userId, user_id: 154938085}))
        // console.log("check follow", await twitchAPI.getFollows(user.twitch.accessToken, {to_id: userId, first: 1}));
        
        // userId = 98742675; // ufa
        // userId = 40298003; // mad
        // this.subscribe("users/follows", {to_id: userId});
    }

    async auth() {
        let {data} = await axios.post("https://id.twitch.tv/oauth2/token", null, {
            params: {
                client_id: twitchAPI.clientID,
                client_secret: twitchAPI.clientSecret,
                grant_type: "client_credentials",
                scope: "channel:read:subscriptions"
            }
        });
        this.token = data;

        this._test();
    }

    // Webhooks HubSub
    async hubRequest({mode, topic, search = {}, lease, accessToken}) {
        if (typeof search === "object" && !search.first) search.first = 1;
        search = new URLSearchParams(search);        
        let res;
        try {
            let topicStr = `${twitchAPI.apiURL}/${topic}?${search}`;
            let {data} = await axios.post(this.hubUrl, {
                "hub.mode": mode,
                "hub.topic": topicStr,
                "hub.callback": this.callback,
                "hub.lease_seconds": lease || 864000,
                // "hub.secret": params["hub.secret"], // TODO
            }, {
                headers: {
                    "Client-ID": twitchAPI.clientID,
                    "Authorization": `Bearer ${accessToken || this.token.access_token}`,
                }
            });
            res = data;
        } catch(e) {
            if (e.response) {
                console.error(`Error Twitch Hub request: ${mode} ${topic}`, e.response.data);
            } else {
                console.error(e);
            }
        }
        return res;
    }

    async subscribe(topic, search, accessToken) {
        await this.hubRequest({mode: "subscribe", topic, search, accessToken, lease: 1*DAY});

        try {
            let hooks = await twitchAPI.getWebhookSubscriptions(this.token.access_token);
            console.log("hooks", hooks);
        } catch(e) {
            if (e.response) console.error("hooks err", e.response.data);
            else console.error("hooks err", e);
        }
    }

    async unsubscribe(topic, search, accessToken) {
        await this.hubRequest({mode: "unsubscribe", topic, search, accessToken});
    }

    async unsubscribeAll() {
        let hooks = await twitchAPI.getWebhookSubscriptions(this.token.access_token);
        console.log("hooks", hooks);
        
        let req = hooks.data.map(async (hook) => {
            let {data} = await axios.post(this.hubUrl, {
                "hub.mode": "unsubscribe",
                "hub.topic": hook.topic,
                "hub.callback": hook.callback,
            }, {
                headers: {
                    "Client-ID": twitchAPI.clientID,
                    "Authorization": `Bearer ${this.token.access_token}`,
                }
            });
        });

        await Promise.all(req);

        hooks = await twitchAPI.getWebhookSubscriptions(this.token.access_token);
        console.log("hooks after", hooks);
    }

    async hubChallenge(ctx) {
        let code = ctx.query["hub.challenge"];
        let topic = ctx.query["hub.topic"];
        let twitchId = topic.match(/_id=(\d+)/)[1];
        console.log("hub challenge", twitchId, code, topic);
        // console.log(ctx.request.url);
        if (!code) console.error(ctx.query);
        ctx.body = code;
    }

    async hubCallback(ctx) {
        let body = ctx.request.body;
        let msg = body && body.data && body.data[0];
        
        if (!msg) {
            console.log("hub callback", body);
            return ctx.body = "ok";
        }
        console.log("hub message", msg);
        if (this.messages[msg.id]) return ctx.body = "ok";
        this.messages[msg.id] = msg;

        if (!msg.event_type && msg.followed_at) msg.event_type = "follow";
        let twitchId;
        if (msg.event_type === "follow") {
            twitchId = msg.to_id;
        } else if (msg.event_type.startsWith("subscription")) {
            twitchId = msg.event_data.broadcaster_id;
        }
        // io.emit("hub", msg);
        io.to(`user:twitch-${twitchId}`).emit("twitch.event", msg);
        ctx.body = "ok";
    }
}

module.exports = new Controller();