const ctrl = require('./controller');
const Router = require('koa-router');
var router = new Router();

router.get('/hub/cb', ctrl.hubChallenge.bind(ctrl));
router.post('/hub/cb', ctrl.hubCallback.bind(ctrl));

module.exports = router;