// const cfg = require('./config');
const idle = (ctx, next) => next();

module.exports.authRequired = (ctx, next) => {
    if (!ctx.isAuthenticated()) return ctx.throw(401);
    if (next) return next();
}

module.exports.adminRequired = (ctx, next) => {
    if (!ctx.isAuthenticated()) return ctx.throw(401);
    if (!ctx.state.user.access.isAdmin) return ctx.throw(403);
    if (next) return next();
}

if (config.ratelimitEnabled) {
    // documentation: https://github.com/koajs/ratelimit
    let ratelimit = require('koa-ratelimit');
    let Redis = require('ioredis');
    let redis = new Redis(config.redis);

    module.exports.rateLimit = (duration = 'high', max) => {
        // duration is in seconds
        switch (duration) {
            case 'high':
                duration = 60; max = 600;
                break;
            case 'low':
                duration = 60; max = 10;
                break;
        }

        return ratelimit({
            db: redis,
            duration: duration * 1000,
            errorMessage: 'Error 429: Too many requests!',
            id: (ctx) => ctx.ip,
            max: max,
            disableHeader: true
        });
    }
} else {
    module.exports.rateLimit = () => idle;
}
