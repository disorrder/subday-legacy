const axios = require('axios');
const User = require("../routes/user/model");

class TwitchAPI {
    constructor() {
        this.apiURL = "https://api.twitch.tv/helix";
        this.api = axios.create({baseURL: this.apiURL});
        this.clientID = config.twitch.clientID;
        this.clientSecret = config.twitch.clientSecret;
        // this._test();
    }

    async _test() {
        // let {data} = await axios.
    }

    async refreshToken(refreshToken, params = {}) {
        let {data} = await axios.post("https://id.twitch.tv/oauth2/token", {}, {
            params: {
                grant_type: "refresh_token",
                client_id: config.twitch.clientID,
                client_secret: config.twitch.clientSecret,
                refresh_token: refreshToken,
                ...params,
            }
        });
        return data;
    }

    async getFollows(accessToken, params) {
        let {data} = await axios.get(`${this.apiURL}/users/follows`, {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
            params: {
                first: 100,
                ...params,
            }
        });
        return data;
    }

    async checkSubscription(accessToken, params) {
        let {data} = await axios.get(`${this.apiURL}/subscriptions`, {
            headers: {
                "Authorization": `Bearer ${accessToken}`,
            },
            params,
        });        
        
        return data;
    }

    async getAllSubscribers(accessToken, params) {
        if (!params.first) params.first = 100;
        let subs = [];
        let finished = false;
        while (!finished) {
            let data = await this.checkSubscription(accessToken, params);
            subs.push(...data.data);
            finished = data.data.length < params.first;

            if (data.pagination) {
                params.after = data.pagination.cursor
            }
        }

        subs = subs.filter(sub => {
            if (sub.user_id === sub.broadcaster_id) return false;
            return true;
        });

        return subs;
    }

    async getSubsCount(accessToken, params) {
        let points = {
            1000: 1,
            2000: 2,
            3000: 6
        };
        let data = {
            count: 0,
            score: 0,
            count_breakdown: {
                1000: 0,
                2000: 0,
                3000: 0,
            },
            score_breakdown: {
                1000: 0,
                2000: 0,
                3000: 0,
            },
            gifts: 0,
        };
        let subs = await this.getAllSubscribers(accessToken, params);
        subs.forEach(sub => {
            data.count++;
            data.score += points[sub.tier];
            data.count_breakdown[sub.tier]++;
            data.score_breakdown[sub.tier] += points[sub.tier];
            if (sub.is_gift) data.gifts++;
        });
        data.subs = subs;
        return data;
    }

    async getWebhookSubscriptions(accessToken, params = {}) {
        console.log("hooks req", accessToken);
        let {data} = await this.api.get("webhooks/subscriptions", {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
            params: {
                // first: 100,
                ...params,
            }
        });
        return data;
    }
}

module.exports = new TwitchAPI();
