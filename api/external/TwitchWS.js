const WebSocket = require('ws');
const EventEmitter = require('events');
const {SEC, MIN} = require("../../utils");
const User = require("../routes/user/model");

class TwitchWS extends EventEmitter {
    constructor() {
        super();
        this.url = "wss://pubsub-edge.twitch.tv";
        this.clientID = config.twitch.clientID;
        this.clientSecret = config.twitch.clientSecret;
    
        this.topics = [];

        // this.connect();
    }
    
    async _test() {
        let user = await User.findById("5d700b3f88944a262f1e04ec").select("+twitch.accessToken"); // Juice
        console.log(user.twitch.login);
        // this.unlisten({
        //     topics: [`channel-subscribe-events-v1.${user.twitch.id}`],
        // })
        this.listen({
            topics: [`channel-subscribe-events-v1.${user.twitch.id}`],
            "auth_token": user.twitch.accessToken
        });
        console.log(this.topics);
        
    }

    connect() {
        this.ws = new WebSocket(this.url, {
            // perMessageDeflate: false
        });

        this.ws.on("open", () => {
            console.log("TwitchWS is opened");
            this.ping();
            this._test();
        });

        this.ws.on('message', this.onMessage.bind(this));

        this.heartbeatI = setInterval(() => {            
            this.ping();
        }, 4.9*MIN);
    }

    send(msg) {
        this.ws.send(JSON.stringify(msg));
    }

    onMessage(msg) {
        msg = JSON.parse(msg);
        this.emit("message", msg);
        this.emit(`message:${msg.type}`, msg);
        if (msg.type === "PONG") this.pong(msg);
        if (msg.type === "MESSAGE") {
            let topic = msg.data.topic;
            // let [topicName, userId] = topic.split(".");
            if (topic.startsWith("channel-subscribe-events")) {
                this.emit("subscription", msg)
            }
        }

        console.log("pub msg", msg);
        
    }

    ping() {
        this._pingStart = Date.now();
        this.send({type: "PING"});
    }

    pong() {
        this._ping = Date.now() - this._pingStart;
        this._pingStart = null;
        console.log("ping", this._ping+"ms");
        this.emit("pong", {ping: this._ping});
    }

    listen(data) {
        data.topics = data.topics.filter(v => !this.topics.includes(v));
        this.topics.push(...data.topics);
        this.emit({type: "LISTEN", data});
    }
    
    unlisten(data) {
        data.topics.forEach(topic => {
            this.topics.splice(this.topics.indexOf(topic), 1);
        });
        this.emit({type: "UNLISTEN", data});
    }
}

module.exports = {
    TwitchWS,
    twitchWS: new TwitchWS(),
};
