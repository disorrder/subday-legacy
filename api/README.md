# starter-vue
Simple Vue starter with webpack

### Install global dependencies
`npm i -g nodemon`

### Install dependencies
`npm i`

### Run locally
`npm run dev` - development mode  
`npm run api` - API  

## Production
`npm run build` - build for production to `.build` folder

# Configs
Для того, чтобы переопределить поля в конфигах, не обязательно копировать конфиг целиком. Достаточно только указать те поля, которые нужно переопределить.  
Дефолтные конфиги лежат в папке `config/`.

# Файловая структура
На сервере выделяется папка `/var/www/bdo-bot`, внутри которой лежат:  
 - `public/` - папка со статичной сборкой проекта
 - `uploads/` - папка с загружаемыми файлами
 - `.logs/` - папка с логами
 - `config/` - папка с локальными конфигами
