// ref https://github.com/playcanvas/engine/blob/master/src/core/color.js
const math = require("./math");

class Color {
    constructor(r, g, b, a) {
        var length = r && r.length;
        if (length === 3 || length === 4) {
            this.r = r[0];
            this.g = r[1];
            this.b = r[2];
            this.a = r[3] !== undefined ? r[3] : 1;
        } else {
            this.r = r || 0;
            this.g = g || 0;
            this.b = b || 0;
            this.a = a !== undefined ? a : 1;
        }
    }

    clone() {
        return new Color(this.r, this.g, this.b, this.a);
    }

    copy(rhs) {
        this.r = rhs.r;
        this.g = rhs.g;
        this.b = rhs.b;
        this.a = rhs.a;

        return this;
    }

    set(r, g, b, a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = (a === undefined) ? 1 : a;

        return this;
    }

    lerp(lhs, rhs, alpha) {
        this.r = lhs.r + alpha * (rhs.r - lhs.r);
        this.g = lhs.g + alpha * (rhs.g - lhs.g);
        this.b = lhs.b + alpha * (rhs.b - lhs.b);
        this.a = lhs.a + alpha * (rhs.a - lhs.a);

        return this;
    }

    fromString(hex) {
        if (!hex) return;
        if (hex.startsWith("rgba")) return this.fromRGBA(hex);
        var i = parseInt(hex.replace('#', '0x'), 16);
        var bytes;
        if (hex.length > 7) {
            bytes = math.intToBytes32(i);
        } else {
            bytes = math.intToBytes24(i);
            bytes[3] = 255;
        }

        this.set(bytes[0] / 255, bytes[1] / 255, bytes[2] / 255, bytes[3] / 255);

        return this;
    }

    toString(alpha) {
        var s = "#" + ((1 << 24) + (Math.round(this.r * 255) << 16) + (Math.round(this.g * 255) << 8) + Math.round(this.b * 255)).toString(16).slice(1);
        if (alpha === true) {
            var a = Math.round(this.a * 255).toString(16);
            if (this.a < 16 / 255) {
                s += '0' + a;
            } else {
                s += a;
            }

        }

        return s;
    }

    toRGBA() {
        return `rgba(${this.r * 255 | 0}, ${this.g * 255 | 0}, ${this.b * 255 | 0}, ${this.a})`;
    }

    fromRGBA(string) {
        if (!string || !string.startsWith("rgba")) return;
        let [r, g, b, a] = string.split(",").map(v => parseFloat(v));
        this.r = r / 255;
        this.g = g / 255;
        this.b = b / 255;
        this.a = a;
    }
}

module.exports = {
    Color,
};
