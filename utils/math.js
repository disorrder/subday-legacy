// --- Randomize ---
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}
Object.assign(module.exports, {getRandomInt, getRandomFloat});

const availableChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
String.randomize = function(len) {
    var str = "";
    for (let i = 0; i < len; i++) {
        str += availableChars.charAt(Math.floor(Math.random() * availableChars.length));
    }
    return str;
};

Array.prototype.getRandom = function() {
    return this[getRandomInt(0, this.length-1)];
};

// --- trigonometry ---
module.exports.DEG_TO_RAD = 180 / Math.PI;
module.exports.RAD_TO_DEG = Math.PI / 180;

// --- playcanvas ---
module.exports.intToBytes24 = function(i) {
    var r, g, b;
    r = (i >> 16) & 0xff;
    g = (i >> 8) & 0xff;
    b = (i) & 0xff;
    return [r, g, b];
};

module.exports.intToBytes32 = function(i) {
    var r, g, b, a;
    r = (i >> 24) & 0xff;
    g = (i >> 16) & 0xff;
    b = (i >> 8) & 0xff;
    a = (i) & 0xff;
    return [r, g, b, a];
};
