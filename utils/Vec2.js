// ref https://github.com/playcanvas/engine/blob/master/src/core/color.js
const math = require("./math");

class Vec2 {
    constructor(x, y) {
        if (x && x.length === 2) {
            this.x = x[0];
            this.y = x[1];
        } else {
            this.x = x || 0;
            this.y = y || 0;
        }
    }

    clone() {
        return new Vec2().copy(this);
    }

    copy(rhs) {
        this.x = rhs.x;
        this.y = rhs.y;
        return this;
    }

    set(x, y) {
        this.x = x;
        this.y = y;
        return this;
    }

    add(rhs) {
        this.x += rhs.x;
        this.y += rhs.y;
        return this;
    }

    add2(lhs, rhs) {
        this.x = lhs.x + rhs.x;
        this.y = lhs.y + rhs.y;
        return this;
    }

    distance(rhs) {
        var x = this.x - rhs.x;
        var y = this.y - rhs.y;
        return Math.sqrt(x * x + y * y);
    }

    dot(rhs) {
        return this.x * rhs.x + this.y * rhs.y;
    }

    equals(rhs) {
        return this.x === rhs.x && this.y === rhs.y;
    }

    length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    lengthSq() {
        return this.x * this.x + this.y * this.y;
    }

    lerp(lhs, rhs, alpha) {
        this.x = lhs.x + alpha * (rhs.x - lhs.x);
        this.y = lhs.y + alpha * (rhs.y - lhs.y);
        return this;
    }

    mul(rhs) {
        this.x *= rhs.x;
        this.y *= rhs.y;
        return this;
    }

    mul2(lhs, rhs) {
        this.x = lhs.x * rhs.x;
        this.y = lhs.y * rhs.y;
        return this;
    }

    normalize() {
        var lengthSq = this.x * this.x + this.y * this.y;
        if (lengthSq > 0) {
            var invLength = 1 / Math.sqrt(lengthSq);
            this.x *= invLength;
            this.y *= invLength;
        }
        return this;
    }

    scale(scalar) {
        this.x *= scalar;
        this.y *= scalar;
        return this;
    }

    sub(rhs) {
        this.x -= rhs.x;
        this.y -= rhs.y;
        return this;
    }

    sub2(lhs, rhs) {
        this.x = lhs.x - rhs.x;
        this.y = lhs.y - rhs.y;
        return this;
    }

    toString() {
        return '[' + this.x + ', ' + this.y + ']';
    }
}

module.exports = {
    Vec2,
};
