class Loading {
    constructor(promises) {
        if (promises) this.add(promises);
    }

    count = 0
    finished = 0
    complete = false

    get progress() {
        return this.finished / this.count;
    }

    get progressPerc() {
        return Math.round(this.progress * 1000) / 10;
    }

    promises = []
    add(promises) {
        this.complete = false;
        promises = promises.filter(v => v instanceof Promise);
        this.count += promises.length;
        promises.map(async (v) => {
            v.finally(() => this.finished++);
            let res = await v;
            return res;
        }).forEach((v) => {
            this.promises.push(v);
        });

        this.promise = Promise.all(this.promises);
        this.promise.finally(() => this.complete = true);
        this.promise.then(() => console.log("complete", this.complete));
    }
}

async function loadFileData(url) {
    const res = await fetch(url);
    const blob = await res.blob();
    const reader = new FileReader();
    return new Promise((resolve, reject) => {
        reader.onloadend = () => resolve(reader.result);
        reader.onerror = reject;
        reader.readAsDataURL(blob);
    });
}

async function loadImageData(url, outputFormat = "image/png") {
    const img = new Image();
    img.crossOrigin = 'anonymous';
    const loadImage = new Promise((resolve, reject) => {
        img.onload = resolve;
        img.onerror = reject;
    });
    
    img.src = url;
    await loadImage;
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    canvas.height = img.naturalHeight;
    canvas.width = img.naturalWidth;
    ctx.drawImage(img, 0, 0);
    return canvas.toDataURL(outputFormat);
}

module.exports = {
    Loading,
    loadFileData,
    loadImageData
};