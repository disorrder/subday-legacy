# subday

## Install and run
### Install global dependencies
No global dependencies

### Install dependencies
`npm i`

### Run locally
`npm run dev` - development mode  

### Run in production
`npm run build` - build for production to `.build` folder

# Configs
Для того, чтобы переопределить поля в конфигах, не обязательно копировать конфиг целиком. Достаточно только указать те поля, которые нужно переопределить.  
Дефолтные конфиги лежат в папке `config/`. Файл для переопределения не попадает в репозиторий и создаётся в папке `src` автоматически при запуске.  

# Файловая структура
На сервере выделяется папка `/var/www/subday`, внутри которой лежат:  
 - `public/` - папка со статичной сборкой проекта
 - `config/` - папка с локальными конфигами
