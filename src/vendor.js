// --- Merge configs ---
import "utils/assignDeep";
{ // merge configs
    require("../config/web");    
    let config_ci = process.env.WEB_CONFIG || {};
    Object.assignDeep(window.config, config_ci, window.localConfig);
    
    // if (config.ws.indexOf("://") < 0) {
    //     config.ws = location.origin + config.ws;
    //     config.ws = config.ws.replace("http", "ws");
    // }
}
// --- Print versions ---
window.VERSION = VERSION;
window.REVISION = REVISION;
console.info("App version is:", VERSION, REVISION, BRANCH);
console.info("Built at", BUILD_DATE, process.env.NODE_ENV);

// --- Libs ---
import axios from 'axios';
window.axios = axios;

import vuetify from "./plugins/vuetify";
import '@mdi/font/css/materialdesignicons.css'
import "@disorrder/timeline";
