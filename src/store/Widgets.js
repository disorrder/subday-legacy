import { VuexModule, Module, Mutation, Action } from "@/plugins/vuex-modules";
import Collection from "./__Collection";

let additional = {
    roulette: {
        icon: "mdi-view-carousel",
        color: "indigo",
        iconStyle: "transform: rotate(90deg);"
    }
}

@Module()
export default class Widgets extends Collection {
    url = "/widget";
    types = [];

    @Action async loadTypes() {
        let {data} = await api.get(`${this.url}-types`);
        data.forEach(v => Object.assign(v, additional[v.name]));
        this.types = data;
        return data;
    }

    remap(item) {
        item.__Collection = this.constructor.name;
        let type = this.types.find(v => v._id === item.type);
        item.type = type;
        return item;
    }

    @Action async getList() {
        if (!this.types.length) await this.loadTypes();
        return super.getList(...arguments);
    }

    @Action async getById() {
        if (!this.types.length) await this.loadTypes();
        return super.getById(...arguments);
    }
}
