import { VuexModule, Module, Mutation, Action } from "@/plugins/vuex-modules";

// Пока не на чем тестить, но должно работать
export default class Collection extends VuexModule {
    id = "_id"; // index field
    url = "/collection";
    items = [];

    @Mutation add(item) {
        if (!item[this.id]) return;
        let index = this.items.findIndex((v) => v[this.id] === item[this.id]);
        item = this.remap(item);
        
        if (index === -1) {
            this.items.push(item);
        } else {
            this.items.splice(index, 1, item);
        }

        return this;
    }

    remap(item) {
        item.__Collection = this.constructor.name;
        if (item.children) {
            item.children.forEach((child) => {
                child._parent = item;
            });
        }
        return item;
    }

    @Mutation remove(item) {
        let index = this.items.findIndex((v) => v[this.id] === item[this.id]);
        this.items.splice(index, 1);
        return this;
    }

    @Mutation removeById(id) {
        let index = this.items.findIndex((v) => v[this.id] === id);
        this.items.splice(index, 1);
        return this;
    }


    @Action async create(data, options) {
        let {data: item} = await api.post(`${this.url}`, data, options);
        this.add(item);
        return item;
    }

    @Action async getList(options) {
        let {data: items} = await api.get(`${this.url}s`, options);
        
        items.forEach((item) => {
            this.add(item);
        });
        return items;
    }

    @Action async getById(id) {        
        let {data: item} = await api.get(`${this.url}/${id}`);
        this.add(item);
        return item;
    }

    @Action async updateById(id, data) {
        let {data: item} = await api.patch(`${this.url}/${id}`, data);
        this.add(item);
        return item;
    }

    @Action async update(item) {
        return this.updateById(item[this.id], item);
    }

    @Action async deleteById(id) {
        let {data} = await api.delete(`${this.url}/${id}`);
        this.removeById(id);
        return data;
    }

    @Action async delete(item) {
        return this.deleteById(item[this.id]);
    }
}
