import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

var store = new Vuex.Store({
    modules: {
        ui: require('./ui').default,
        user: require('./user').default,
        users: require("./Users").default,
        polls: require("./Polls").default,
        games: require("./Games").default,
        // roulettes: require("./Roulettes").default,
        widgets: require("./Widgets").default,
    },
});

export default store;
