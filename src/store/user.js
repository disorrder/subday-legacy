export default {
    namespaced: true,
    state: {
        isAuthenticated: false,
        user: {},
    },
    
    mutations: {
        login(state, data) {
            state.isAuthenticated = true;
            state.user = data;
            localStorage.lastLogin = data.username;
            console.log("login", data);
            
            if (state.user.twitch) {
                twitchApi.defaults.headers.Authorization = `Bearer ${state.user.twitch.accessToken}`;
            }
        },
        logout(state) {            
            state.isAuthenticated = false;
            state.user = {};
            state.characters = [];
            console.log("logout");
        },
    },

    actions: {
        async login({commit}, creds) {
            let {data} = await api.post("/auth/login", creds);
            commit("login", data);
            return data;
        },

        async logout({commit}) {
            let {data} = await api.get("/auth/logout");
            commit("logout");
            return data;
        },

        async updateUser({commit}) {
            try {
                var {data} = await api.get("/user/me");
                commit("login", data);
            } catch(e) {
                console.warn("UpdateUser error", e);
                commit("logout");
            }
            return data;
        },

        async isAuthenticated({ dispatch, state }) {
            if (state.isAuthenticated) return true;
            await dispatch("updateUser");
            return state.isAuthenticated;
        },

        async check({}, params) {
            let {data} = await api.get("/user/check", {params});
            return data;
        },

        async save({commit}, item) {
            let {data} = await api.patch(`user/${item._id}`, item);
            commit("login", data);
            return data;
        },

        // twitch
        async checkTwitchFollow({commit}, ownerId) {
            let {data} = await api.get(`user/twitch/check-follow/${ownerId}`);
            return data;
        },
        async checkTwitchSubscription({commit}, ownerId) {
            let {data} = await api.get(`user/twitch/check-sub/${ownerId}`);
            return data;
        },
        async getTwitchSubsCount({commit}, options) {
            let {data} = await api.get("/user/twitch/get-subs-count", options);
            return data;
        },
    }
}