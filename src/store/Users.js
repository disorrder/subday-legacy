import { VuexModule, Module, Mutation, Action } from "@/plugins/vuex-modules";
import store from "@/store";
import Collection from "./__Collection";

@Module()
export default class Users extends Collection {
    url = "/user";

    @Action async saveTwitch(item) {
        let {data} = await api.patch(`/user/${item._id}/twitch`);
    }
}
