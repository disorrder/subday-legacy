export default {
    namespaced: true,
    state: {
        transparent: false,
        navbar: { enabled: true },
        footer: { enabled: true },
    },
    
    mutations: {

    },

    actions: {

    }
}