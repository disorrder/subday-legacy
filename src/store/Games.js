import { VuexModule, Module, Mutation, Action } from "@/plugins/vuex-modules";

@Module()
export default class Games extends VuexModule {
    steam = []

    @Mutation setSteam(items) {
        this.steam = [...items];
    }

    @Action async getSteam() {
        let {data} = await api.get("/steam-api/games");
        this.setSteam(data);
        return data;
    }
}
