import { VuexModule, Module, Mutation, Action } from "@/plugins/vuex-modules";
import Collection from "./__Collection";

@Module()
export default class Polls extends Collection {
    url = "/poll";

    @Action async getListByCategory(category = "my") {
        let {data: items} = await api.get(`${this.url}s/${category}`);        
        items.forEach((item) => {
            this.add(item);
        });
        return items;
    }

    @Action async publish(item) {
        let {data} = await api.post(`${this.url}/${item[this.id]}/publish`);
        this.add(data);
        return data;
    }

    @Action async unpublish(item) {
        let {data} = await api.post(`${this.url}/${item[this.id]}/unpublish`);
        this.add(data);
        return data;
    }

    @Action async finish(item) {
        let {data} = await api.post(`${this.url}/${item[this.id]}/finish`);
        this.add(data);
        return data;
    }

    @Action async vote([item, option]) {
        let {data} = await api.post(`${this.url}/${item[this.id]}/vote`, option);
        this.add(data);
        return data;
    }

    @Action async setOwner([item, user]) {
        let {data} = await api.put(`${this.url}/${item[this.id]}/owner`, {id: user._id});
        this.add(data);
        return data;
    }

    @Action async getVotes(id) {
        let {data} = await api.get(`${this.url}/${id}/votes`);
        return data;
    }

}
