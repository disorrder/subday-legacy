import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
import "./style.styl";
import ru from 'vuetify/src/locale/ru.ts';

Vue.use(Vuetify);

export default new Vuetify({
    lang: {
        locales: {ru},
        current: "ru",
    },
    theme: {
        themes: {
            light: {
                twitch: "#6441a5",
                discord: "#7289da",
            },
        }
    },
});

