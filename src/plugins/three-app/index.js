import * as THREE from "three";
import EE from "events";
import "./Object3D";
export {default as ScriptType} from "./ScriptType";

export class Application extends EE {
    constructor() {
        super();

        this.timer = {
            time: 0,
            dt: 0,
        };

        this.root = new THREE.Scene();
        this.root._app = this;
        this.renderer = new THREE.WebGLRenderer({antialias: true});
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.init();
        this.gameLoop();
    }

    init() {
        let root = this.root;
        let player, camera;

        player = new THREE.Group();
        player.position.set(0, 0, 5);
        this.player = player;
        
        camera = new THREE.PerspectiveCamera(70, 1, 0.01, 1000);
        camera.lookAt(root.position);
        this.camera = camera;
        player.camera = camera;
        player.add(camera);

        root.add(player);
    }

    gameLoop(time) {
        this.timer.dt = time - this.timer.time;
        this.timer.time = time;
        this.emit("tick", this.timer.dt);
        this.renderer.render(this.root, this.camera);
        requestAnimationFrame(this.gameLoop.bind(this));
    }
    
    appendTo(el) {
        el.appendChild(this.renderer.domElement);
        this.updateAspect();
    }
    
    updateAspect() {
        let el = this.renderer.domElement;
        this.camera.aspect = el.clientWidth / el.clientHeight;
        this.camera.updateProjectionMatrix();
        this.renderer.setSize(el.clientWidth, el.clientHeight);        
    }
}