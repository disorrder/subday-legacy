import {Object3D} from "three";

Object3D.prototype.getParents = function() {
    let parents = [];
    this.traverseAncestors((obj) => {
        parents.push(obj);
    });
    return parents;
};

Object3D.prototype.getRoot = function() {
    let parents = this.getParents();
    return parents[parents.length - 1];
};
