import EE from "events";

export default class ScriptType extends EE {
    constructor(app, object) {
        super();
        this.app = app;
        this.object = object;
        
        if (!object.scripts) object.scripts = [];
        object.scripts.push(this);

        this._enabled = true;

        this.app.on("tick", (dt) => {
            if (!this.enabled) return;
            this.tick(dt);
        });
    }

    get enabled() {
        return this._enabled;
    }
    set enabled(val) {
        this._enabled = val;
        if (val && !this.initialized) this.initialize();
    }

    initialize() {
        if (this._initialized) return;
        if (!this.enabled) return;
        this._initialized = true;
    }

    tick() {}

}