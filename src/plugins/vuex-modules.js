// think of name: vuex-oop, vuex-class-decorator, vuex-class
import "utils/String";

function define(Class) {
    let superDef = Class.definition || {};
    Class.definition = {
        // name: null,
        namespaced: true,
        state: {...superDef.state},
        getters: {...superDef.getters},
        actions: {...superDef.actions},
        mutations: {...superDef.mutations},
    };
};

const moduleParams = {
    namespaced: true,
};

export class VuexModule {
    constructor() {
        
    }
}

export function Module(params = {}) {
    params = {...moduleParams, ...params};
    return function(Class) {
        if (!Class.hasOwnProperty("definition")) define(Class);
        let module = new Class();
        let def = Class.definition;
        def.__Class = Class;
        def.__module = module;
        def.state = module;

        let descriptors = Object.getOwnPropertyDescriptors(module.__proto__);
        for (let name in descriptors) {
            if (descriptors[name].get) {
                def.getters[name] = function(state, getters) {
                    return descriptors[name].get.call(this);
                };
            }
        }

        ["getters", "actions", "mutations"].forEach((k) => {
            let group = def[k]
            for (let name in group) {
                group[name] = group[name].bind(module);
            }
        });

        if (params.namespaced) {
            def.namespaced = params.namespaced;
            def.name = params.name || Class.name.uncapitalize();
        }
        
        if (params.store) params.store.registerModule(def.name, def);
        return def;
    }
}

export function Mutation(target, name, descriptor) {
    let Class = target.constructor;
    if (!Class.hasOwnProperty("definition")) define(Class);
    let def = Class.definition;

    def.mutations[name] = function(state, payload) {
        return descriptor.value.call(this, payload);
    };
}

export function Action(target, name, descriptor) {
    let Class = target.constructor;
    if (!Class.hasOwnProperty("definition")) define(Class);
    let def = Class.definition;

    def.actions[name] = function(ctx, payload) {
        return descriptor.value.call(this, payload);
    };    
}