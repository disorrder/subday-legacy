uniform float size;
uniform float t1;
uniform float t2;
uniform float maxOffset;
// uniform float time;

attribute float offset;
attribute vec3 color;
attribute vec3 morph1;
attribute vec3 morph2;

varying vec3 vPosition;
varying vec3 vColor;
varying float vt1;
varying float vt2;

void main() {
    vColor = color;
    
    vPosition = position;
    float _offset = offset * maxOffset;
    
    // morph1
    if (t1 > _offset || t1 < -_offset) {
        vt1 = (t1 - _offset) / (1.0 - maxOffset);
        vPosition = mix(vPosition, morph1, vt1);
    } else {
        vt1 = 0.0;
    }
    
    // morph2
    if (t2 > _offset || t2 < -_offset) {
        vt2 = (t2 - _offset) / (1.0 - maxOffset);
        vPosition = mix(vPosition, morph2, vt2);
    } else {
        vt2 = 0.0;
    }

    gl_PointSize = size;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(vPosition, 1.0);
}
