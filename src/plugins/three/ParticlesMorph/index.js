import * as THREE from "three";
import { Points, ShaderMaterial, Vector3, Float32BufferAttribute, BufferGeometry, Color } from "three";
import vs from "./shader.vert";
import fs from "./shader.frag";
import {getRandomFloat} from "utils";

export class ParticlesMorph extends Points {
    constructor(geometry, material) {
        if (!geometry) geometry = new BufferGeometry();
        if (!material) material = new MatParticlesMorph();
        super(geometry, material);

        this.updateAttrs();
    }

    updateAttrs() {
        this.geometry.computeBoundingBox();
        let position = this.geometry.attributes.position;
        this.verticesCount = position ? position.count : 0;
        this.updateOffset();
        this.updateColor();
        this.updateMorph1();
        this.updateMorph2();
    }

    updateOffset() {
        if (!this.verticesCount) return;
        let attr = new Float32BufferAttribute(new Float32Array(this.verticesCount * 1), 1);
        for (let i = 0, len = attr.count; i < len; i++) {
            attr.setX(i, Math.random());
        }
        this.geometry.setAttribute("offset", attr);
    }

    updateColor() {
        if (!this.verticesCount) return;
        let position = this.geometry.attributes.position;
        let attr = new Float32BufferAttribute(new Float32Array(this.verticesCount * 3), 3);
        let bbSize = this.geometry.boundingBox.getSize(new Vector3());
        let p = new Vector3();
        let c = new Color();
        let x;
        for (let i = 0, len = attr.count; i < len; i++) {
            p.set(position.getX(i), position.getY(i), position.getZ(i));
            x = (p.x - this.geometry.boundingBox.min.x) / bbSize.x;
            x += getRandomFloat(-0.03, 0.03);
            c.setHSL(x, 1, 0.5);
            attr.setXYZ(i, c.r, c.g, c.b);
        }
        this.geometry.setAttribute("color", attr);
    }

    updateMorph1() {
        if (!this.verticesCount) return;
        let position = this.geometry.attributes.position;
        let attr = new Float32BufferAttribute(new Float32Array(this.verticesCount * 3), 3);
        let bbSize = this.geometry.boundingBox.getSize(new Vector3());
        let p = new Vector3();
        for (let i = 0, len = attr.count; i < len; i++) {
            p.set(position.getX(i), position.getY(i), position.getZ(i));
            let sign = Math.random() > 0.5 ? 1 : -1;
            p.x += sign * getRandomFloat(bbSize.x * 1, bbSize.x * 2);
            p.y *= 3;
            attr.setXYZ(i, p.x, p.y, p.z);
        }

        this.geometry.setAttribute("morph1", attr);
        // geometry.morphAttributes.position[0] = attr;
    }

    updateMorph2() {
        if (!this.verticesCount) return;
        let position = this.geometry.attributes.position;
        let attr = new Float32BufferAttribute(new Float32Array(this.verticesCount * 3), 3);
        let bbSize = this.geometry.boundingBox.getSize(new Vector3());
        let p = new Vector3();
        for (let i = 0, len = attr.count; i < len; i++) {
            p.set(position.getX(i), position.getY(i), position.getZ(i));
            p.y -= getRandomFloat(bbSize.y * 1, bbSize.y * 2);
            attr.setXYZ(i, p.x, p.y, p.z);
        }

        this.geometry.setAttribute("morph2", attr);
        // geometry.morphAttributes.position[1] = attr;
    }
}

export class MatParticlesMorph extends ShaderMaterial {
    constructor(params = {}) {
        params = {
            vertexShader: vs,
            fragmentShader: fs,
            color: 0x999999,
            uniforms: {
                size: {type: "f", value: 2},
                t1: {type: "f", value: 0},
                t2: {type: "f", value: 0},
                maxOffset: {type: "f", value: 0.7},
            },
            blending: THREE.AdditiveBlending,
            depthTest: true,
            depthWrite: false,
            transparent: true,
            ...params,
        };
        super(params);
    }
}