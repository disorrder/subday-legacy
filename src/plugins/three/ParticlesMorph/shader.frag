uniform float size;
uniform float t1;
uniform float t2;
uniform float maxOffset;
// uniform float time;

varying vec3 vPosition;
varying vec3 vColor;
varying float vt2;

void main() {
    float opacity = 0.5 * (1.0 - abs(vt2));
    gl_FragColor = vec4(vColor, opacity);
    // gl_FragColor = color;
}
