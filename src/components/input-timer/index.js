import './style.styl';
import Vue from 'vue';
import colors from 'vuetify/lib/util/colors';
import {SEC, MIN, HOUR, DAY} from "utils";

Vue.component('input-timer', {
    props: {
        value: Number,
        hideMs: Boolean,
        hideS: Boolean,
        hideD: Boolean,
    },
    template: require('./template.pug')(),
    data() {
        return {
            ms: "00",
            s: "00",
            m: "00",
            h: "00",
            d: "00",
            shouldUpdate: true,
        }
    },
    computed: {

    },
    watch: {
        value() {
            if (!this.shouldUpdate) return;
            this.updateInputs();
        }
    },
    methods: {
        async onInput(name) {
            this[name] = this[name].replace(/\D+/g, "");            

            let val;
            val = +this.d * DAY;
            val += +this.h * HOUR;
            val += +this.m * MIN;
            val += +this.s * SEC;
            val += +this.ms;
            this.shouldUpdate = false;
            this.$emit("input", val);
            await this.$nextTick();
            this.shouldUpdate = true;
        },

        updateInputs() {
            this.ms = this.value % SEC;
            this.s = Math.floor(this.value % MIN / SEC);
            this.m = Math.floor(this.value % HOUR / MIN);
            this.h = Math.floor(this.value % DAY / HOUR);
            this.d = Math.floor(this.value / DAY);
            // ["ms", "s", "m", ]
        }
    },
    mounted() {
        this.updateInputs();
    }
});
