import './style.styl';
import Vue from 'vue';
Vue.component('navbar', {
    template: require('./template.pug')(),
    data() {
        return {
            navItems: [

            ]
        }
    },
    computed: {
        visible() { return this.$store.state.ui.navbar.enabled },
        isAuthenticated() { return this.$store.state.user.isAuthenticated; },
        user() { return this.$store.state.user.user; },
        isStreamer() { return this.isAuthenticated && this.user.access.isStreamer; },
    },
});
