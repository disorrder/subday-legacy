import './style.styl';
import Vue from 'vue';
Vue.component('owner-view', {
    props: {
        id: String,
        hideImage: Boolean, 
    },
    template: require('./template.pug')(),
    data() {
        return {
            loading: false,
        }
    },
    computed: {
        user() { return this.$store.state.users.items.find(v => v._id === this.id); },
    },
    async mounted() {
        if (!this.user) {
            this.loading = true;
            await this.$store.dispatch("users/getById", this.id);
            this.loading = false;
        }
    }
});
