import './style.styl';
import Vue from 'vue';
import colors from 'vuetify/lib/util/colors';
import {SEC, MIN, HOUR, DAY} from "utils";

Vue.component('input-position', {
    props: {
        value: Object,
    },
    template: require('./template.pug')(),
    data() {
        return {
            buttons: [
                { icon: "mdi-arrow-top-left-bold-outline", value: {x: "start", y: "start"}, angle: "135deg" },
                { icon: "mdi-arrow-up-bold-outline", value: {x: "center", y: "start"}, angle: "90deg" },
                { icon: "mdi-arrow-top-right-bold-outline", value: {x: "end", y: "start"}, angle: "45deg" },
                { icon: "mdi-arrow-left-bold-outline", value: {x: "start", y: "center"}, angle: "180deg" },
                { icon: "mdi-circle-outline", value: {x: "center", y: "center"} },
                { icon: "mdi-arrow-right-bold-outline", value: {x: "end", y: "center"}, angle: "0deg" },
                { icon: "mdi-arrow-bottom-left-bold-outline", value: {x: "start", y: "end"}, angle: "135deg" },
                { icon: "mdi-arrow-down-bold-outline", value: {x: "center", y: "end"}, angle: "90deg" },
                { icon: "mdi-arrow-bottom-right-bold-outline", value: {x: "end", y: "end"}, angle: "45deg" },
            ],
        }
    },
    computed: {
        activeBtn() {
            if (!this.value) return null;
            return this.buttons.find(btn => {
                return this.value.x === btn.value.x && this.value.y === btn.value.y;    
            });
        }
    },
    methods: {
        async onInput(btn) {
            this.$emit("input", {...btn.value});
        },
    },
    mounted() {

    }
});
