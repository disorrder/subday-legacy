import Vue from "vue";
Vue.directive("slider-fix", {
    inserted(el) {
        // console.log("slider-fix insert", el, this, el.__vue__);
        
    },
    update(el) {
        let vm = el.__vue__;
        let thumb = el.querySelector(".v-slider__thumb-container");
        let trackFill = el.querySelector(".v-slider__track-fill");
        let trackBackground = el.querySelector(".v-slider__track-background");
        thumb.style.left = vm.value + "%";
        setTimeout(() => {
            thumb.style.left = vm.value + "%";
            trackFill.style.width = vm.value + "%";
            trackBackground.style.width = 100 - vm.value + "%";
        }, 0);
    }
})