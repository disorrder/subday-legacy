import './style.styl';
import Vue from 'vue';
import colors from 'vuetify/lib/util/colors';
import {getRandomInt} from "utils";

Vue.component('twitch-sub', {
    props: {
        value: Object,
    },
    template: require('./template.pug')(),
    data() {
        return {

        }
    },
    computed: {
        color() {
            if (this.value.tier === 1000) return "teal";
            if (this.value.tier === 2000) return "green";
            if (this.value.tier === 3000) return "orange";
        },
        tierClass() { return `tier--${this.value.tier}` },
    },
    created() {

    }
});
