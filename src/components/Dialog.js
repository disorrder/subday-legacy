export default class Dialog {
    constructor() {
        this.enabled = false;
        this.data = {};
    }

    open(data = {}) {
        this.data = {...data};
        this.enabled = true;
    }
    
    close() {
        this.enabled = false;
        this.data = {};
    }
}
