import Vue from "vue";

// Date
let __date = new Date();
const TIMEZONE = __date.getTimezoneOffset() * 60 * 1000;


Vue.filter("time", (v) => {
    if (!v) return "";
    __date = new Date(v);
    return  __date.toLocaleTimeString();
});
Vue.filter("time-hhmm", (v) => {
    if (!v) return "";
    __date = new Date(v);
    let time =  __date.toLocaleTimeString().split(":");
    time.pop();
    return time.join(":");
});

Vue.filter("date", (v) => {
    if (!v) return "";
    __date = new Date(v);
    return __date.toLocaleDateString();
});
Vue.filter("date-ddmm", (v) => {
    if (!v) return "";
    __date = new Date(v);
    let date = __date.toLocaleDateString().split(".");
    date.pop();
    return date.join(".");
});

Vue.filter("date-time", (v) => {
    if (!v) return "";
    __date = new Date(v);
    let date = __date.toLocaleDateString();
    let time = __date.toLocaleTimeString().split(":");
    time.pop();
    time = time.join(":");
    return `${date} ${time}`;
});
