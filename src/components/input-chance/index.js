import './style.styl';
import Vue from 'vue';

Vue.component('input-chance', {
    props: {
        value: Array,
    },
    template: require('./template.pug')(),
    data() {
        return {

        }
    },
    computed: {
        chancesSum() { return this.value && this.value.reduce((sum, v) => sum + v.value, 0); },
    },
    methods: {
        async onInputChance(chance) {
            if (typeof chance.value === "string") chance.value = parseFloat(chance.value);
            let sum = this.chancesSum;
            if (sum > 100) {
                chance.value -= sum - 100;
                await this.$nextTick();
                this.$emit("input", [...this.value]);
            }
        },
    },
});
