import './style.styl';
import Vue from 'vue';
Vue.component('app-footer', {
    template: require('./template.pug')(),
    data() {
        return {

        }
    },
    computed: {
        visible() { return this.$store.state.ui.footer.enabled },
        isAuthenticated() { return this.$store.state.user.isAuthenticated; },
        user() { return this.$store.state.user.user; },
    },
});
