import './style.styl';
import Vue from 'vue';
import anime from "animejs";
import math from "utils/math";
import {Loading} from "utils/Loading";
import {sleep} from "utils";

class Sector {
    item = null
    angle = 0
    offset = 0
    isWinner = false

    style = {}
}

Vue.component('w-roulette', {
    props: ["item"],
    template: require('./template.pug')(),
    data() {
        return {
            cacheImages: [],
            loading: null,
            radius: 300,
            sectorsCount: 10,
            sectorsRange: [-Math.PI * 3/4, Math.PI * 3/4],
            angle: 0,
            speed: 0,
            startSpeed: 2.5,
            spinning: false,

            sectors: [],
        }
    },
    computed: {
        sectorAngle() { return 2 * Math.PI / this.sectorsCount; },
        sectorStyle() { return {
                top: `calc(50% - ${this.sectorHeight/2}px)`,
                height: this.sectorHeight+"px",
            };
        },
        sectorHeight() { return 1.1 * Math.abs(this.radius * 2 * Math.sin(this.sectorAngle/2)); },
        sectorDistance() { return 1.1 * Math.abs(this.radius * Math.cos(this.sectorAngle/2)); },
        direction() {
            if (this.speed > 0) return 1;
            if (this.speed < 0) return -1;
            return 0;
        },
        rarityMap() {
            return this.item.settings.rarityTable.map((rarity, i) => {
                let sum = 0;
                for (let j = 0; j <= i; j++) {
                    sum += this.item.settings.rarityTable[j].value;
                }
                return sum;
            });
        },
        groupedOptions() {
            let res = this.item.settings.rarityTable.map(v => []);
            this.item.settings.options.forEach(v => {
                res[v.rarity-1].push(v);
            });
            return res;
        },
    },
    methods: {
        sectorPositionStyle(sector) {
            let angle = sector.angle;
            return {
                transform: `translate3d(0, ${this.sectorDistance * Math.sin(-angle)}px, ${this.sectorDistance * Math.cos(angle) - this.sectorDistance}px) rotateX(${angle}rad)`,
                filter: `brightness(${Math.abs(Math.cos(angle))})`,
            }
        },
        tick(dt) {
            if (this.spinning) {
                this.angle += this.speed * dt / 1000;
                this.updateSectors();
            }
        },
        updateSectors() {            
            this.sectors.forEach((sector, i) => {
                sector.angle = this.angle + sector.offset;
                sector.style = {
                    ...sector.style,
                    ...this.sectorPositionStyle(sector),
                };                

                // replace sectors out of range
                if (this.speed < 0) {
                    if (sector.angle < this.sectorsRange[0]) {
                        sector.offset += 2 * Math.PI;
                        sector.option = this.selectOption();
                    }
                }
                if (this.speed > 0) {
                    if (sector.angle > this.sectorsRange[1]) {
                        sector.offset -= 2 * Math.PI;
                        sector.option = this.selectOption();
                    }
                }
            });
        },
        createSector(offset) {
            // offset is required
            let sector = new Sector();
            sector._offset = sector.offset = offset;
            sector.angle = this.angle + sector.offset;
            sector.option = this.selectOption();
            return sector;
        },
        getCurrentSector() {
            let minAbsAngle, res;
            this.sectors.forEach((sector, i) => {
                let angle = Math.abs(sector.angle);
                if (i === 0 || angle < minAbsAngle) {
                    minAbsAngle = angle;
                    res = sector;
                };
            });
            return res;
        },
        async spin() {
            if (this.spinning) return;
            this.spinning = true;
            this.speed = math.getRandomFloat(this.startSpeed, this.startSpeed*1.5);
            let fallback = math.getRandomFloat(1, 1.5);
                        
            this.sectors.forEach(sector => {
                sector.isWinner = false;
            });

            await anime({
                targets: this,
                delay: 5000,
                duration: 40000,
                easing: `cubicBezier(0.700, 0.195, 0.610, ${fallback})`,
                // easing: "cubicBezier(0.850, 0.560, 0.860, 1.220)",
                // easing: "easeInOutQuad",
                speed: 0,
            }).finished;
            
            let angle = Math.round(this.angle / this.sectorAngle) * this.sectorAngle;
            
            await anime({
                targets: this,
                delay: 1000,
                duration: 500,
                easing: "cubicBezier(0.700, 0.195, 0.610, 1.270)",
                // easing: "easeInOutQuad",
                angle
            }).finished;

            let sector = this.getCurrentSector();
            if (sector) {
                sector.isWinner = true;
            }

            this.spinning = false;
        },
        selectOption() {
            let r = math.getRandomInt(0, 100);
            let i = this.rarityMap.findIndex(v => r <= v);            
            let rarity = this.item.settings.rarityTable[i];
            let option = this.groupedOptions[i].getRandom();
            return {
                ...option,
                color: rarity.color
            };
        },

        // sector style
        sectorTitleStyle(sector) {
            let style = {};
            if (sector.option.title.length > 20) style.fontSize = "0.75em";
            if (sector.option.title.length > 30) style.fontSize = "0.55em";
            return style;
        },

        // events
        run() {
            return this.spin();
        },
        onResize(e) {
            let w = this.$el.clientWidth;
            this.radius = w / 2;
            this.updateSectors();
        }
    },
    async created() {
        // preload images
        let preloadImages = this.item.settings.options.filter(v => v.image).map(async (option) => {
            return new Promise((resolve, reject) => {
                let img = new Image();
                this.cacheImages.push(img);
                img.onload = resolve;
                img.onerror = reject;
                img.src = option.image;
                return img;
            }).catch(img => {
                console.error("Can't load image", option.image, `in option [${option.title}]`);
            });
        });
        this.loading = new Loading(preloadImages);
        await this.loading.promise;
        await sleep(300);
        
        // generate sectors
        for (let i = 0; i < this.sectorsCount ; i++) {
            let offset = i * this.sectorAngle - Math.PI;
            let sector = this.createSector(offset);
            this.sectors.push(sector);
        }
        this.onResize();
        
        this.$root.$on("tick", this.tick.bind(this));
    },
    mounted() {
        this.$on("run", this.run.bind(this));
        window.addEventListener("resize", this.onResize.bind(this));
        this.onResize();
    },
});
