uniform float size;
uniform float time;
uniform float duration;

attribute vec3 position2;

varying vec3 vPosition;

void main() {
    vPosition = position;
    gl_PointSize = size;
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
