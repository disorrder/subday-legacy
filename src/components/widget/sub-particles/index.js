import './style.styl';
import Vue from 'vue';
import {SEC, MIN, sleep, getRandomFloat} from "utils";
import {Application} from "../../../plugins/three-app";
import * as GeometryUtils from "../../../plugins/three-app/GeometryUtils";
import * as THREE from "three";
import {ParticlesMorph} from "../../../plugins/three/ParticlesMorph"

// import vertexShader from "./material/particles.vert";
// import fragmentShader from "./material/particles.frag";

Vue.component('w-sub-particles', {
    props: ["item"],
    template: require('./template.pug')(),
    data() {
        return {
            text: "KappaPride",
            particlesCount: 50000,
        }
    },
    computed: {
        users() { return this.$store.state.users.items },
        owner() { return this.users.find(v => v._id === this.item.owner) },
    },
    methods: {
        async init() {
            this.app3d = new Application();
            this.app3d.appendTo(this.$refs.container);

            window.addEventListener("resize", () => {
                this.app3d.updateAspect();
                this.updateCamera();
            });

            let root = this.app3d.root;
            let player = this.app3d.player;

            player.position.set(0, 1, 5);
            player.camera.lookAt(root.position);

            let light;
            light = new THREE.AmbientLight(0x404040); // soft white light
            root.add(light);
    
            light = new THREE.DirectionalLight(0xff0000, 1);
            player.add(light);

            this.font = await this.loadFont("https://raw.githubusercontent.com/mrdoob/three.js/master/examples/fonts/helvetiker_regular.typeface.json");
            this.textObject = new ParticlesMorph();
            this.updateText();
            root.add(this.textObject);

            // Animate
            // this.textObject.material.uniforms.t1.value = 1;
            this.animation = new Timeline()
            .add({
                delay: 1000,
                duration: 3000,
                animate: [{
                    target: this.textObject.material.uniforms.t1,
                    from: {value: 1},
                    to: {value: 0}
                }],
                easing: Timeline.easing.QuarticInOut,
            })
            .add({
                delay: 2000,
                duration: 3000,
                animate: [{
                    target: this.textObject.material.uniforms.t2,
                    from: {value: 0},
                    to: {value: 1}
                }],
                easing: Timeline.easing.QuarticIn,
            });
        },

        async loadFont(url) {
            let loader = new THREE.FontLoader();
            return new Promise((resolve, reject) => {
                loader.load(url, resolve);
            });
        },

        updateText() {
            let textGeometry = this.getTextGeometry();
            let points = GeometryUtils.randomPointsInGeometry(textGeometry, this.particlesCount);
            let geometry = this.textObject.geometry;
            
            let attr;
            attr = new THREE.BufferAttribute(new Float32Array(this.particlesCount * 3), 3);
            points.forEach((p, i) => {
                attr.setXYZ(i, p.x, p.y, p.z);
            });
            geometry.setAttribute("position", attr);

            this.textObject.updateAttrs();

            this.updateCamera();
            // this.updateBB(); // debug
        },

        updateCamera() {
            // place camera to fit bb
            let player = this.app3d.player;
            let geometry = this.textObject.geometry;
            geometry.computeBoundingBox();
            let bb = geometry.boundingBox;
            let bbSize = new THREE.Vector3();
            bb.getSize(bbSize).multiplyScalar(0.5);
            let distance = player.position.length();

            let tanY = Math.tan(THREE.Math.degToRad(player.camera.fov) / 2);
            let tanX = tanY * player.camera.aspect;
            let dist = Math.max(bbSize.x / tanX, bbSize.y / tanY) * 1.2;
            
            player.position.multiplyScalar(dist / distance);
        },

        getTextGeometry() {
            let textGeometry = new THREE.TextGeometry(this.text, {
                font: this.font,
                size: 2,
                height: 0.3
            });
            textGeometry.center();
            return textGeometry;
        },

        updateBB() {
            if (!this.bbMesh) {
                this.bbMesh = new THREE.Mesh(
                    new THREE.BoxGeometry(),
                    new THREE.MeshBasicMaterial({color: 0x990000, wireframe: true})
                );
            }
            console.log("bbMesh", this.bbMesh);
            
            let geometry = this.textObject.geometry;
            geometry.computeBoundingBox();
            let bb = geometry.boundingBox;
            let size = new THREE.Vector3();
            bb.getSize(size);

            this.textObject.bbMesh.geometry.scale(size.x, size.y, size.z);
            this.textObject.bbMesh.visible = true;
        },

        play() {
            this.textObject.material.uniforms.t1.value = 1;
            this.textObject.material.uniforms.t2.value = 0;
            this.animation.stop().play();
        },
        
        test() {
            this.text = "Subscriber";
            this.updateText();
            this.play();
        },

        twitchEvent(msg) {
            let name;
            if (msg.event_type === "follow") {
                name = msg.from_name;
            } else if (msg.event_type.startsWith("subscription")) {
                name = msg.event_data.user_name;
            }
            if (name) {
                this.text = name;
                this.updateText();
                this.play();
                console.log("twitch", msg.event_type, msg);
            }
        },
    },
    async created() {
        this.$on("test", this.test.bind(this));
        ws.on("twitch.event", this.twitchEvent.bind(this));

    },
    async mounted() {
        this.init();
    },
});














