import './style.styl';
import Vue from 'vue';
import {SEC, MIN, HOUR, DAY} from "utils";

Vue.component('w-timer', {
    props: ["item"],
    template: require('./template.pug')(),
    data() {
        return {
            d: 0,
            h: 0,
            m: 0,
            s: 0,
            ms: 0,
            time: 654654,
            startedAt: null,
        }
    },
    computed: {
        timeStr() {
            this.d = Math.floor(this.time / DAY);
            this.h = Math.floor(this.time % DAY / HOUR);
            this.h = this.h.toString().padStart(2, "0");
            this.m = Math.floor(this.time % HOUR / MIN);
            this.m = this.m.toString().padStart(2, "0");
            this.s = Math.floor(this.time % MIN / SEC);
            this.s = this.s.toString().padStart(2, "0");
            this.ms = this.time % SEC;
            this.ms = this.ms.toString().padStart(3, "0");

            return `${this.h}:${this.m}:${this.s}`
        },
        isPlaying() {
            return this.item.settings.startedAt;
        },
        showMs() {
            return this.time < 10 * SEC;
        },
        isRed() {
            return this.time < 10 * SEC && this.time % 1000 > 500;
        }
    },
    methods: {
        tick() {
            if (!this.item.settings.startedAt) return;
            if (!this.startedAt) this.startedAt = new Date(this.item.settings.startedAt);
            
            let dt = Date.now() - this.startedAt;
            let time = this.item.settings.timeLeft;
            this.time = time - dt;
            if (this.time < 0) {
                this.time = 0;
                return this.pause();
            }
            requestAnimationFrame(this.tick.bind(this));
        },
        play(data) {            
            if (!this.item.settings.timeLeft) this.item.settings.timeLeft = this.item.settings.amount;
            this.item.settings.startedAt = data.startedAt;
            this.startedAt = new Date(data.startedAt);
            this.$store.state.widgets.update(this.item);
            this.tick();
        },
        pause() {
            this.item.settings.timeLeft = this.time;
            this.item.settings.startedAt = this.startedAt = null;
            this.$store.state.widgets.update(this.item);
        },
        stop() {
            this.item.settings.timeLeft = this.item.settings.amount;
            this.item.settings.startedAt = this.startedAt = null;
            this.time = this.item.settings.amount;
            this.$store.state.widgets.update(this.item);
        }
    },
    async created() {

    },
    mounted() {
        this.$on("play", this.play.bind(this));
        this.$on("pause", this.pause.bind(this));
        this.$on("stop", this.stop.bind(this));
        this.time = this.item.settings.timeLeft || this.item.settings.amount;
        this.tick();
    },
});
