import './style.styl';
import Vue from 'vue';
import {SEC, MIN} from "utils";

Vue.component('w-subscore', {
    props: ["item"],
    template: require('./template.pug')(),
    data() {
        return {
            subData: {},
        }
    },
    computed: {
        users() { return this.$store.state.users.items },
        owner() { return this.users.find(v => v._id === this.item.owner) },
        text() {
            let text = this.item.settings.text || "";            
            if (this.subData.count == null) this.subData.count = "–";
            if (this.subData.score == null) this.subData.score = "–";            
            return text
                .replace("$score", this.subData.score)
                .replace("$count", this.subData.count)
            ;
        },
        hasSubVar() {
            return this.item.settings.text.includes("$count")
                || this.item.settings.text.includes("$score")
            ;
        },

        textClass() {
            return {
                shadow: this.item.styles.shadow,
                "clip-bg": this.item.styles.gradient,
            }
        }
    },
    methods: {
        async updateSubs() {
            if (!this.hasSubVar) return;
            this.subData = await this.$store.dispatch("user/getTwitchSubsCount", {params: {user_id: this.item.owner}});
            console.info("update subs", this.subData);
        },

        twitchEvent(msg) {
            if (msg === "subscriptions.notification") {
                console.log("twitch sub", msg);
                this.updateSubs();
            }
        },
    },
    async created() {
        this.$on("refresh", this.updateSubs.bind(this));
        ws.on("twitch.event", this.twitchEvent.bind(this));
        // if (!this.owner) await this.$store.state.users.getById(this.item.owner);
        // this.subData = this.owner.twitch.subscribers || {};
        await this.updateSubs();

        setInterval(() => {
            this.updateSubs();
        }, 5*MIN);
    },
    mounted() {

    },
});
