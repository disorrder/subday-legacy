import './style.styl';
import Vue from 'vue';
import {getRandomInt, getRandomFloat} from "utils";
import {Vec2} from "utils/Vec2";
// import {Color} from "utils/Color";

const PI2 = Math.PI * 2;

Vue.component('w-snow', {
    props: ["item"],
    template: require('./template.pug')(),
    data() {
        return {
            particles: [],
            __time: 0,
            __prevTime: 0,
            velocityScale: 50,

            layers: [
                {
                    filter: "blur(1px)",
                },
                {
                    filter: "blur(2px)",
                },
                {
                    filter: "blur(3px)",
                },
            ]
        }
    },
    computed: {
        velocity() {
            return new Vec2(...this.item.settings.velocity);
        },
        velocityThreshold() {
            return new Vec2(...this.item.settings.velocityThreshold);
        },
        clientSize() {
            return new Vec2(this.$el.clientWidth, this.$el.clientHeight);
        },
    },
    watch: {
        "item.settings.count"() { this.update(); }
    },
    methods: {
        animate(time = 0) {
            // if (!this.item.settings.startedAt) return;
            this.__time = time;
            this.tick(time - this.__prevTime);
            this.draw();
            this.__prevTime = time;
            requestAnimationFrame(this.animate.bind(this));
        },
        tick(dt) {
            let pos = new Vec2();
            this.particles.forEach(part => {
                if (part.time > this.__time) return;                
                pos.copy(part.velocity).scale(dt/1000).scale(1 - part.layer * 0.3);
                part.position.add(pos);
                
                // if (!this.item.settings.startedAt) return; // TODO
                if (part.position.y > this.clientSize.y * 1.1) {
                    this.spawn(part);
                }
            });
            
        },
        draw() {
            this.particles.sort((a, b) => b.layer - a.layer);

            let canvas = this.$refs.layer1;
            let ctx = canvas.getContext("2d");
            
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.fillStyle = this.item.styles.color;
            let layerId, layer;
            this.particles.forEach((part, i) => {
                if (layerId !== part.layer) {
                    layerId = part.layer;
                    layer = this.layers[layerId];
                    ctx.beginPath();                    
                }
                
                ctx.moveTo(part.position.x, part.position.y);
                ctx.arc(part.position.x, part.position.y, part.radius, 0, PI2);

                if (!this.particles[i+1] || this.particles[i+1].layer !== layerId) {
                    ctx.filter = layer.filter;
                    ctx.fillStyle = layer.fillStyle;
                    ctx.fill();
                }
            });
        },

        update() {
            let canvas = this.$refs.layer1;
            canvas.width = canvas.clientWidth;
            canvas.height = canvas.clientHeight;

            let items = this.particles;
            this.particles = [];
            let part;
            let maxT = this.clientSize.y / this.velocity.y * this.velocityScale;
            
            for (let i = 0; i < this.item.settings.count; i++) {
                part = items[i];
                if (!part) {
                    part = this.spawn();
                    part.time += 1000+getRandomInt(0, maxT);
                }
                this.particles.push(part);
            }
        },

        spawn(part = {}) {
            part.time = this.__time || 0;
            part.position = new Vec2(getRandomFloat(-0.3, 1.3), -0.1)
                .mul(this.clientSize)
            ;
            part.velocity = new Vec2(Math.random() - 0.5, Math.random() - 0.5)
                .mul(this.velocityThreshold)
                .add(this.velocity)
                .scale(this.velocityScale)
            ;
            part.radius = getRandomFloat(2, 4);
            part.layer = this.item.settings.parallax ? getRandomInt(0, 2) : 0;
            return part;
        },


        play(data) {            
            if (!this.item.settings.timeLeft) this.item.settings.timeLeft = this.item.settings.amount;
            this.item.settings.startedAt = data.startedAt;
            this.startedAt = new Date(data.startedAt);
            this.$store.state.widgets.update(this.item);
            this.tick();
        },
        stop() {
            this.item.settings.timeLeft = this.item.settings.amount;
            this.item.settings.startedAt = this.startedAt = null;
            this.time = this.item.settings.amount;
            this.$store.state.widgets.update(this.item);
        }
    },
    async created() {

    },
    mounted() {
        this.$on("play", this.play.bind(this));
        this.$on("stop", this.stop.bind(this));
        this.update();
        this.animate();
    },
});
