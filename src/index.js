import "./vendor";
// --- Sources ---
import './style.styl';
import './routes/theme.styl';

import './components';

import Vue from 'vue';
import store from './store';
import router from './routes';
import vuetify from "./plugins/vuetify";
import Notifications from 'vue-notification';
Vue.use(Notifications);
import {debounce} from "../utils/common";
Vue.prototype.$debounce = debounce;

// Vue.config.productionTip = false;

window.onload = () => {
    var app = new Vue({
        el: '#app',
        store,
        router,
        vuetify,
        data: {
        },
        computed: {
            isAuthenticated() { return this.$store.state.user.isAuthenticated; },
            user() { return this.$store.state.user.user; },
            isStreamer() { return this.isAuthenticated && this.user.access.isStreamer; },    
            isAdmin() { return this.isAuthenticated && this.user.access.isAdmin; },    
        },
        methods: {
            notify_notImplemented() {
                return this.$notify({type: "error", text: "Пока не работает"});
            },
        },
        async created() {
            this._animation = new Animation(this);

            await this.$store.dispatch('user/updateUser');
        }
    });
    window.app = app;
};

window.api = axios.create({
    baseURL: config.api,
});
window.twitchApi = axios.create({
    baseURL: config.twitch,
});
import io from "socket.io-client";
window.ws = io();


class Animation {
    constructor(vm) {
        this.vm = vm;
        this.dt = 0;
        this.lastTime = performance.now();
        this.tick();
    }

    tick(time) {
        requestAnimationFrame(this.tick.bind(this));
        this.dt = time - this.lastTime;
        this.lastTime = time;
        if (!this.dt) return;
        this.vm.$emit("tick", this.dt);
    }
}
