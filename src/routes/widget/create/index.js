import './style.styl';
import { mapActions } from 'vuex';

export default {
    template: require('./template.pug')(),
    // components: {...require("./components")},
    data() {
        return {
            loading: false,
            creating: false,
        }
    },
    computed: {
        user() { return this.$root.user; },
        widgetTypes() { return this.$store.state.widgets.types; },
    },
    methods: {
        ...mapActions({
            updateWidget: "widgets/update",
            getWidget: "widgets/getById",
        }),

        async create(type) {            
            let widget = await this.$store.state.widgets.create({type: type._id});
            this.$router.push({name: "widget.edit", params: {id: widget._id}});
        },
    },
    async created() {
        await this.$store.state.widgets.loadTypes();
    },
};
