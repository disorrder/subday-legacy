import './style.styl';
import { mapActions } from 'vuex';
// import {sleep} from "utils";
import Dialog from "../../../components/Dialog";

export default {
    template: require('./template.pug')(),
    components: {...require("./components")},
    data() {
        return {
            loading: true,
            widgetLinkShow: false,

            loadingUI: {
                save: false,
                cancel: false,
                delete: false,
            },

            deleteDialog: new Dialog(),
        }
    },
    computed: {
        itemId() { return this.$route.params.id; },
        item() { return this.$store.state.widgets.items.find(v => v._id === this.itemId) },
        user() { return this.$root.user; },
        owner() { return this.$store.state.users.items.find(v => v._id === this.item.owner) },
        isOwner() { return this.item.owner === this.user._id; },

        editTemplate() { return "widget-edit-"+this.item.type.name; },

        widgetLink() {
            let routeData = this.$router.resolve({name: "wigdet.view", params: this.$route.params});
            return location.origin + routeData.href;
        }
    },
    methods: {
        ...mapActions({
            updateWidget: "widgets/update",
            getWidget: "widgets/getById",
        }),

        openWidget() {            
            window.open(this.widgetLink+"?debug=1", '_blank');
        },

        async copyWidgetLink(e) {
            if (!this.widgetLink) return;
            await navigator.clipboard.writeText(this.widgetLink);
            this.$notify({type: "success", title: "Скопировано в буфер обмена"})
        },

        sendAction(method, data = {}) {
            let options = {widgetId: this.item._id, method, data};
            this.$emit("widget.action", options);
            ws.emit("widget.action", options);
            this.$refs.widget.$emit(method, data);
        },

        async save() {
            this.loadingUI.save = true;
            await this.updateWidget(this.item);
            this.loadingUI.save = false;
            ws.emit("widget.action", {widgetId: this.item._id, method: "update"});
            this.$notify({type: "success", title: "Успешно сохранено"});
        },

        async cancel() {
            this.loadingUI.cancel = true;
            await this.getWidget(this.item._id);
            this.$router.push({name: "widget-list"});
            this.loadingUI.cancel = false;
        },

        async remove() {
            this.loadingUI.delete = true;            
            await this.$store.state.widgets.delete(this.item);
            this.$router.push({name: "widget-list"});
            this.loadingUI.delete = false;
        },
    },
    async created() {
        this.loading = true;
        await this.getWidget(this.itemId);
        if (!this.owner) await this.$store.state.users.getById(this.item.owner);
        this.loading = false;
        
        if (!this.isOwner && !this.user.access.isAdmin) {
            this.$router.push({name: "widget-list"});
            this.$notify({type: "error", text: "Доступ запрещён"});
            return;
        }

        setTimeout(() => {
            window.scrollTo({top: 0, behavior: "smooth"});
        });
    },
};
