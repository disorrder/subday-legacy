import "./style.styl";

export default {
    template: require('./template.pug')(),
    data() {
        return {
            background: {},
        }
    },
    computed: {
        item() { return this.$parent.item; },
        timeStr: {
            get() {
                return this.msToString(this.item.settings.amount);
            },
            set(str) {
                this.item.settings.amount = this.msFromString(str);
            }
        }
    },
    methods: {
        inputBackground() {
            this.item.styles.background = this.rgbaToString(this.background);            
        },

        rgbaToString(color) {
            let {r, g, b, a} = color;
            let str = [r, g, b, a]
                .map(v => Math.round(v*100)/100)
                .join(", ")
            ;
            return `rgba(${str})`;
        },
        rgbaFromString(string) {
            let [r, g, b, a] = string
                .replace("rgba(", "")
                .replace(")", "")
                .split(",")
                .map(v => parseFloat(v))
            ;
            return {r, g, b, a};
        },

        onActionPlay(options) {
            let time = new Date().toISOString();
            this.item.settings.startedAt = time;
            options.data.startedAt = time;
        }
    },
    created() {
        this.background = this.rgbaFromString(this.item.styles.background);
        this.$parent.$on("widget.action", (options) => {
            if (options.method === "play") this.onActionPlay(options)
        });
    },
    mounted() {
        
    }
}
