import "./style.styl";

export default {
    template: require('./template.pug')(),
    data() {
        return {
            background: {},
        }
    },
    computed: {
        item() { return this.$parent.item; },

    },
    methods: {

    },
    created() {
        this.$parent.$on("widget.action", (options) => {
            if (options.method === "play") this.onActionPlay(options)
        });
    },
    mounted() {
        
    }
}
