import "./style.styl";

export default {
    template: require('./template.pug')(),
    data() {
        return {
            // loadingUI: {
            //     save: false,
            //     cancel: false,
            // },

            imageDialog: {
                enabled: false,
                data: null,
            },
        }
    },
    computed: {
        item() { return this.$parent.item; },
    },
    methods: {
        async addOption() {
            this.item.settings.options.push({
                title: "",
                rarity: 1,
            });
        },

        deleteOption(option) {
            let i = this.item.settings.options.indexOf(option);
            this.item.settings.options.splice(i, 1);
        },



        openImageDialog(option) {
            this.imageDialog.enabled = true;
            this.imageDialog.data = option;
        },

        saveImage() {
            this.imageDialog.enabled = false;
        },
        removeImage() {
            this.imageDialog.data.image = null;
            this.imageDialog.enabled = false;
        },
    },
    mounted() {
        
    }
}