import "./style.styl";
import {Color} from "utils/Color";

export default {
    template: require('./template.pug')(),
    data() {
        return {
            color: new Color(),
        }
    },
    computed: {
        item() { return this.$parent.item; },
        colorInput: {
            get() { return this.color; },
            set(v) {
                this.color.r = v.r / 255;
                this.color.g = v.g / 255;
                this.color.b = v.b / 255;
                this.color.a = v.a;
                this.item.styles.color = this.color.toRGBA();
            }
        },
    },
    methods: {
        inputColor() {
            console.log("col", this.color);
            this.item.styles.color = this.color.toRGBA();            
        },

        onActionPlay(options) {
            let time = new Date().toISOString();
            this.item.settings.startedAt = time;
            options.data.startedAt = time;
        }
    },
    created() {
        this.color.fromString(this.item.styles.color);
        this.$parent.$on("widget.action", (options) => {
            if (options.method === "play") this.onActionPlay(options)
        });
    },
    mounted() {
        
    }
}
