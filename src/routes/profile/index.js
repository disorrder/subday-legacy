import './style.styl';
import { mapActions } from 'vuex';

export default {
    template: require('./template.pug')(),
    data() {
        return {
            confirmationValid: true,
            confirmRules: {
                required: v => !!v || "Поле обязательно для заполнения",
                username: [
                    v => this.confirmError === "ERR_USERNAME_BUSY" ? "Логин занят" : true,
                ],
                email: [
                    v => /.+@.+\..+/.test(v) || "Неправильный адрес",
                    v => this.confirmError === "ERR_EMAIL_BUSY" ? "Email уже используется" : true,
                ],
            },
            confirmError: null,
            subsCount: null,
            showIntro: false,
        }
    },
    computed: {
        user() { return this.$store.state.user.user; },
        subScoreText() {
            if (!this.subsCount) return "";
            if (this.subsCount.score > 200) return "довольно успешный";
            if (this.subsCount.score > 50) return "начинающий";
        },
        showInvite() {
            if (this.user.access.isStreamer) return false;
            return this.subsCount && this.subsCount.score >= 50;
        },
        showInviteSimple() {
            if (this.user.access.isStreamer) return false;
            return this.subsCount && this.subsCount.score < 50;
        },
        twitchBlack: {
            get() { return this.user.twitch.blacklist.join(", ") },
            set(str) { this.user.twitch.blacklist = str.split(/,\s*/gi).map(v => v.trim()) }
        },
    },
    methods: {
        ...mapActions({
            _logout: "user/logout",
            checkUser: "user/check",
            saveUser: "user/save",
        }),

        async remapUser() {
            if (!this.user._id) return;
            if (!this.user.access.isRegistered) {
                if (!this.user.username) {
                    if (this.user.twitch) this.user.username = this.user.twitch.login;
                }
                if (!this.user.email) {
                    if (this.user.twitch) this.user.email = this.user.twitch.email;
                }
            }
            let lastUpdate = this.user.twitch.subscribers && this.user.twitch.subscribers.lastUpdate || 0;
            let dt = Date.now() - new Date(lastUpdate);            
            if (!this.user.access.isStreamer || dt > 1000*60*60*24) {
                this.subsCount = await this.$store.dispatch("user/getTwitchSubsCount");
            }
        },

        async confirmUser() {
            this.$refs.confirm.resetValidation();
            this.confirmError = null;
            if (!this.$refs.confirm.validate()) return;
            this.$notify({title:"valid"});

            let {username, email} = this.user;
            let res = await this.checkUser({username, email});
            if (res) this.confirmError = res;
            if (!this.$refs.confirm.validate()) return;
            await this.saveUser(this.user);
            this.$notify({type: "success", title: "Сохранено"});
        },
        
        async joinClub() {
            try {
                await api.get("/user/request-streamer");
                this.user.access.isStreamer = true;
                this.showIntro = true;
            } catch(e) {
                this.$notify({type: "error", title: "Что-то пошло не так"});
                throw e;
            }
        },

        // saveTwitch() {

        // },
        
        async logout() {            
            await this._logout();
            this.$router.push({name: "auth.login"});
        },
    },
    watch: {
        user() {
            this.remapUser();
        }
    },
    created() {
        this.remapUser();
    },
};
