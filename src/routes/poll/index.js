import './style.styl';
import { mapActions } from 'vuex';

export default {
    template: require('./template.pug')(),
    mixins: [
        require("./mixins").default,
    ],
    data() {
        return {
            loading: true,
            viewTab: 0, // enum of view, stats?

            follow: null,
            subscription: null,

            videos: [
                "dQw4w9WgXcQ?autoplay=1", // Rickroll
                "s_3ATmupvCQ?start=49&autoplay=1", // Toss a coin to your witcher
                "bS4Q-WWyl3Q?start=49&autoplay=1", // Witcher metal
                "uLkuJvLgrIg?start=111&autoplay=1", // Witcher fat metal
                "qFXk7KYAa0A?start=181&autoplay=1", // Witcher fat metal
            ]
        }
    },
    computed: {
        isFollower() { return !!this.follow; },
        isSubscriber() { return !!this.subscription; },
        isAllowed() {
            if (this.canEdit) return true; 
            return this.isFollower && this.item.participants.followers
            || this.isSubscriber && this.item.participants.subscribers
        },

        videoUrl() {
            // return "https://www.youtube.com/embed/" + this.videos[4]; // Debug
            return "https://www.youtube.com/embed/" + this.videos.getRandom();
        },
    },
    methods: {
        ...mapActions({
            getPoll: "polls/getById",
            getUser: "users/getById",
            // getSteamGames: "games/getSteam",
        }),

        async checkFollow() {
            try {
                let data = await this.$store.dispatch("user/checkTwitchFollow", this.owner.twitch.id);
                console.info("follow:", data);
                this.follow = data[0];
            } catch(e) {
                console.warn("Ошибка фолловов", e.response);
                this.$notify({type: "error", title: "Не удалось проверить фолловы"});
            }
        },

        async checkSubscription() {
            try {
                let data = await this.$store.dispatch("user/checkTwitchSubscription", this.owner._id);
                console.info("subscription:", data);
                this.subscription = data[0];
            } catch(e) {
                console.warn("Ошибка подписки", e.response);
                this.$notify({type: "error", title: "Не удалось проверить подписку"});
            }
        },
    },
    async created() {
        await this.getPoll(this.itemId);
        await this.getUser(this.item.owner);
        await this.checkFollow();
        await this.checkSubscription();
        await this.$nextTick();

        if (!this.isStarted && this.canEdit) this.$router.push({name: "poll.edit", params: this.$route.params});
        this.loading = false;        
    },
};
