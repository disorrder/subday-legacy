import { mapActions } from 'vuex';

export default {
    computed: {
        user() { return this.$store.state.user.user; },
        itemId() { return this.$route.params.id; },
        item() { return this.$store.state.polls.items.find(v => v._id === this.itemId) },
    
        owner() { return this.item && this.$store.state.users.items.find(v => v._id === this.item.owner) },

        isOwner() { return this.user._id === this.item.owner; },
        isStarted() { return this.item.publishedAt && new Date(this.item.publishedAt) < Date.now(); },
        isFinished() { return this.item.finishAt && new Date(this.item.finishAt) < Date.now(); },
        isGoing() { return this.isStarted && !this.isFinished; },
        myVote() { return this.item.votes.find(v => v.user === this.user._id); },
        isVoted() { return !!this.myVote; },
        isHidden() {
            if (this.canEdit) return false;
            return this.item.hideResults;
        },
        // canEdit() { return false; },
        canEdit() { return this.isOwner || this.$root.isAdmin; },
    },
    methods: {
        ...mapActions({
            getPoll: "polls/getById",
            updatePoll: "polls/update",
            publishPoll: "polls/publish",
            unpublishPoll: "polls/unpublish",
            finishPoll: "polls/finish",
            deletePoll: "polls/delete",

            getUser: "users/getById",
        }),
    },
};
