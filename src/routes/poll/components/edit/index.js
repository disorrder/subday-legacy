import './style.styl';
import { mapActions } from 'vuex';

export default {
    template: require('./template.pug')(),
    mixins: [
        require("../../mixins").default,
    ],
    data() {
        return {
            loadingUi: {
                save: false,
                cancel: false,
            },

            types: ["default", "games"],
            participants: ["follower", "subscriber"],

            ownerDialog: {
                enabled: false,
                search: "",
            },
        }
    },
    computed: {
        filteredUsers() {
            return this.$store.state.users.items.filter((user) => {
                return user.twitch.display_name.includes(this.ownerDialog.search)
                    || user.twitch.login.includes(this.ownerDialog.search)
                ;
            });
        }
    },
    watch: {
        "item.type"() {
            switch (this.item.type) {
                case "default":
                    this.item.additive = false;
                    this.item.multiple = false;
                    break;
                    
                case "games":
                    this.item.additive = true;
                    this.item.multiple = false;
                    break;

                default:
                    break;
            }
        }
    },
    methods: {
        ...mapActions({
            updatePoll: "polls/update",
            pollSetOwner: "polls/setOwner",
            getUsers: "users/getList",
        }),

        async save() {
            this.loadingUi.save = true;
            await this.updatePoll(this.item);
            this.loadingUi.save = false;
            this.$router.push({name: "poll.view", params: {id: this.itemId}});
        },
        
        async cancel() {
            this.loadingUi.cancel = true;
            await this.getPoll(this.itemId);
            this.loadingUi.cancel = false;
            this.$router.push({name: "poll.view", params: {id: this.itemId}});
        },

        async ownerDialogOpen() {
            if (!this.$root.isAdmin) return;
            this.ownerDialog.enabled = true;
            await this.getUsers();
        },

        async setOwner(user) {
            // this.item.owner = user._id;
            await this.pollSetOwner([this.item, user]);
            this.ownerDialog.enabled = false;
        },

    },
    async created() {
        if (!this.item.type) this.item.type = "default";
    },
};
