import './style.styl';
import { mapActions } from 'vuex';
import { debounce } from "utils/common";
import Dialog from "../../../../components/Dialog";

export default {
    template: require('./template.pug')(),
    mixins: [
        require("../../mixins").default,
    ],
    data() {
        return {
            date1: {
                dateMenu: false,
                timeMenu: false,
                date: "",
                time: "",
            },

            loadingUi: {
                publish: false,
                unpublish: false,
                finish: false,
                remove: false,
            },

            searchGamesUi: {
                str: "",
                pending: false,
                steam: [],
                steamCount: 0,
            },

            resView: 0,
            votesView: {
                headers: [
                    {text: "Имя", value: "user.twitch.display_name"},
                    {text: "Голосовал за", value: "option.title"},
                    {text: "Время", value: "timestamp"},
                ],
                itemsPerPage: -1,
            },
            votes: [],

            deleteDialog: new Dialog(),
        }
    },
    computed: {
        steamGames() { return this.$store.state.games.steam; },
        steamGamesFiltered() { //?
            if (this.searchGamesUi.str.length < 1) {
                return [];
            }
            let str = this.searchGamesUi.str.toLowerCase();
            let res = this.steamGames.filter((v) => v.name.toLowerCase().includes(str));
            this.searchGamesUi.steamCount = res.length;
            if (res.length > 100) res.length = 100;
            return res;
        },
        myOption() {
            if (!this.isVoted) return;
            return this.item.options.find(v => v._id === this.myVote.option);
        },
        results() {
            let res = [...this.item.options];
            let option;
            this.item.votes.forEach((vote) => {
                option = res.find(v => v._id === vote.option);
                if (!option) return console.warn("option not found!", vote);
                if (!option.votes) option.votes = 0;
                option.votes++;
                option.progress = option.votes / this.item.votes.length * 100;
            });
            res.sort((a, b) => b.votes - a.votes);
            return res;
        },
        description() {
            return this.item.description.replace(/\n/gi, "<br>");
        }
    },
    methods: {
        ...mapActions({
            getPoll: "polls/getById",
            votePoll: "polls/vote",
            getPollVotes: "polls/getVotes",
            getUser: "users/getById",
            getUsers: "users/getList",
            getSteamGames: "games/getSteam",
        }),

        async publish() {
            this.loadingUi.publish = true;
            await this.publishPoll(this.item);
            this.loadingUi.publish = false;
        },

        async unpublish() {
            this.loadingUi.unpublish = true;
            await this.unpublishPoll(this.item);
            this.loadingUi.unpublish = false;
        },

        async finish() {
            this.loadingUi.finish = true;
            await this.finishPoll(this.item);
            this.loadingUi.finish = false;
        },

        async remove() {
            this.loadingUi.remove = true;
            await this.deletePoll(this.item);
            this.loadingUi.remove = false;
            this.$router.push({name: "poll-list"});
        },

        debounce,
        async searchGames() {
            if (this.searchGamesUi.str.length < 1) {
                this.searchGamesUi.steam = [];
                return;
            }
            this.searchGamesUi.pending = true;
            let str = this.searchGamesUi.str.toLowerCase();
            let res = this.steamGames.filter((v) => v.name.toLowerCase().includes(str));
            this.searchGamesUi.steamCount = res.length;
            if (res.length > 30) res.length = 30;
            this.searchGamesUi.steam = res;
            this.searchGamesUi.pending = false;
        },

        async voteGame(title) {
            let option = this.item.options.find(v => v.title === title);
            if (!option) option = {title};
            await this.votePoll([this.item, option]);
        },
    },
    async created() {
        if (!this.steamGames.length) await this.getSteamGames();
        // mb create method for getting populated votes?
        this.votes = await this.getPollVotes(this.itemId);
        this.votes.forEach((vote) => {
            vote.option = this.item.options.find(v => v._id === vote.option);
        });
    },
};
