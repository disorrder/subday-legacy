import './style.styl';
import {sleep} from "utils";

export default {
    template: require('./template.pug')(),
    data() {
        return {
            userData: {
                username: '',
                password: '',
            },
            formDisabled: false,
            twitchBtnDisplay: false
        }
    },
    methods: {
        async loginAnimation(name) {
            let _colors;
            if (name === "twitch") {
                _colors = Object.values(colors.purple);
            }

            this.bgCtrl.objects.forEach(obj => {
                let row = this.bgCtrl.rows/2 - obj.coords[1];
                let nextColor = new Color().fromString(_colors.getRandom());
                obj.animation = {
                    name: "Lerp color",
                    timeFrom: this.bgCtrl.__time + row * 40,
                    duration: 300,
                    from: {
                        color: obj.color.clone(),
                    },
                    to: {
                        color: nextColor,
                    }
                };
            });
        },

    },
    
    created() {
        this.$store.state.ui.navbar.enabled = false;
    },
    async mounted() {
        this.bgCtrl = new BgTriangles(this.$refs.canvas);
        
        await sleep(5000);
        this.twitchBtnDisplay = true;
    },
    deleted() {
        this.$store.state.ui.navbar.enabled = true;
    }
};

import colors from "vuetify/lib/util/colors"
import {Color} from "utils/Color"

class BgTriangles {
    constructor(canvas) {
        this.canvas = canvas;
        this.ctx = canvas.getContext("2d");

        this.objects = [];
        this.triangleSize = 70;
        this._i = [1, 0];
        this._j = [Math.cos(Math.PI/3), Math.sin(Math.PI/3)];
        this.offset = []; // сдвиг для того, чтобы в центре сходилось 6 треугольников
        // this.colors = Object.values(colors).map(v => v.base).filter(v => v);        
        // this.colors = [ ...Object.values(colors.blue) ];
        this.colors = [
            colors.blue.base,
            colors.blue.lighten1,
            colors.blue.lighten2,
            colors.blue.lighten3,
            colors.blue.lighten4,
            colors.blue.lighten5,
        ];

        this.twitchBtn = {
            enabled: false,
            triangles: [],
            trianglesCoord: [
                [1, -1], [1, 0], [0, 0],
                [-1, 0], [-1, -1], [0, -1], 
            ],
        };

        this.updateGrid();
        this.updateTwitchBtn();

        this.blinking = {
            // maxCount: 20, //?
            delay: 200,
            duration: 2000,
            lastTime: 0,
        };
        
        window.addEventListener("resize", () => {
            this.updateGrid();
        });

        this._stopAnimation = false;
        this.animate();

        // appear twitch btn
        let anim = new Timeline()
        .add({ delay: 2000 });

        this.twitchBtn.triangles.forEach(obj => {
            obj.animation = anim;
            obj.scale = 1.02; // чтобы убрать полоски
            anim.add({
                delay: -700,
                duration: 1000,
                animate: [{
                    target: obj.color,
                    to: new Color().fromString("#6441a5"),
                }]
            })
        });

        anim.add({
            complete: () => {
                this.twitchBtn.enabled = true;
            }
        });
        anim.play();
    }
    
    updateGrid(force) {
        this.canvas.width = this.canvas.clientWidth;
        this.canvas.height = this.canvas.clientHeight;
        this.offset = [this.canvas.width/2, this.canvas.height/2];
        let size = this.triangleSize;
        let h = size * Math.sin(Math.PI/3);
        this.trianglePointsEven = [
            [0, -h/2],
            [-size/2, h/2],
            [size/2, h/2],
        ];
        this.trianglePointsOdd = [
            [0, h/2],
            [-size/2, -h/2],
            [size/2, -h/2]
        ];

        let xCount = Math.ceil(this.canvas.width / size) * 2;
        let yCount = Math.ceil(this.canvas.height / h);
        yCount += yCount % 2;
        this.cols = xCount;
        this.rows = yCount;

        let id = 0, x, y, points, obj;
        let _objects = this.objects;
        this.objects = [];
        for (let j = -yCount/2; j < yCount/2; j++) {
            y = j * h + h/2;
            for (let i = -xCount/2; i < xCount/2; i++) {
                x = i * size/2;

                obj = _objects.find(v => v.coords[0] === i && v.coords[1] === j);
                if (!force && obj) {
                    this.objects.push(obj);
                    continue;
                };

                if ((i+j) % 2 === 0) {
                    points = this.trianglePointsEven.map(v => [...v]);
                } else {
                    points = this.trianglePointsOdd.map(v => [...v]);
                }

                let color = new Color().fromString(this.colors.getRandom());
                // color.a = 0.8;
                obj = {
                    id: id++,
                    coords: [i, j],
                    position: [x, y],
                    rotation: 0,
                    scale: 1,
                    color,
                    points,
                };
                this.objects.push(obj);
            }
        }
    }

    updateTwitchBtn() {
        this.twitchBtn.triangles = this.twitchBtn.trianglesCoord.map(v => {
            return this.objects.find(obj => obj.coords[0] === v[0] && obj.coords[1] === v[1]);
        });
    }

    tick(time) {
        // Blink animation on each triangle
        if (time > this.blinking.lastTime + this.blinking.delay) {
            for (let i = 0; i < 5; i++) {
                // let obj = this.objects.filter(v => !v.animation).getRandom();
                let obj = this.objects.getRandom();
                if (obj.animation) continue;

                let nextColor = new Color().fromString(this.colors.getRandom());
                // nextColor.a = 0.8;
                obj.animation = {
                    name: "Lerp color",
                    timeFrom: time,
                    duration: this.blinking.duration,
                    from: {
                        color: obj.color.clone(),
                    },
                    to: {
                        color: nextColor,
                    }
                };
                
                this.blinking.lastTime = time;
            }
        }

        let t, dt;
        this.objects.forEach(obj => {
            if (!obj.animation || !obj.animation.name) return;
            dt = time - obj.animation.timeFrom;
            if (dt < 0) return;
            if (dt > obj.animation.duration) dt = obj.animation.duration;
            t = dt / obj.animation.duration;
            // process
            obj.color.lerp(obj.animation.from.color, obj.animation.to.color, t);
            // complete
            if (dt === obj.animation.duration) {
                obj.animation = null;
            }
        });
    }


    // --------------
    animate(time = 0) {
        if (this._stopAnimation) return;
        this.__time = time;
        this.tick(time);
        this.draw();
        requestAnimationFrame(this.animate.bind(this));
    }

    draw() {
        let ctx = this.ctx;
        ctx.clearRect(-this.canvas.width, -this.canvas.height, 2*this.canvas.width, 2*this.canvas.height)
        
        ctx.setTransform(new DOMMatrix())
        ctx.translate(...this.offset);

        // ctx.strokeStyle = "rgba(255, 0, 0, 0.9)";
        // ctx.lineWidth = 1;

        this.objects.forEach(obj => {            
            ctx.save();
            if (obj.position) ctx.translate(...obj.position);
            if (obj.rotation) ctx.rotate(obj.rotation);
            if (obj.scale) ctx.scale(obj.scale, obj.scale);

            ctx.beginPath();
            obj.points.forEach((point, i) => {
                if (!i) ctx.moveTo(...point);
                    else ctx.lineTo(...point);
            });
            ctx.closePath();

            ctx.fillStyle = obj.color.toRGBA();
            ctx.fill();

            ctx.restore();
        });
    }
}