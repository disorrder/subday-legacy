import './style.styl';

export default {
    // $_veeValidate: {
    //     validator: 'new'
    // },
    template: require('./template.pug')(),
    data() {
        return {
            userData: {
                username: '',
                email: '',
                password: '',
                password2: '',
            },
            formDisabled: false,
            // showAgreement: false,
        }
    },
    methods: {
        register() {
            if (this.formDisabled) return;
            this.formDisabled = true;

            this.$validator.validateAll()
                .then((res) => {
                    if (!res) throw res;
                    return api.post('/user/add', this.userData);
                })
                .then((user) => {
                    this.$store.commit('login', user);
                    this.$router.push({name: 'profile'});
                    this.$notify({type: 'success', title: 'Добро пожаловать!', text: 'Не забудь подтвердить почту ;)'});
                })
                .catch((res) => {
                    console.log('err valid', res, this.errors);
                    if (!res) return;
                    switch (res.responseText) {
                        case 'ERR_USERNAME_BUSY': return this.errors.add({field: 'username', rule: 'unique', msg: true});
                        case 'ERR_EMAIL_BUSY': return this.errors.add({field: 'email', rule: 'unique', msg: true});
                        case 'ERR_EMAIL_INCORRECT': return this.errors.add({field: 'email', rule: 'incorrect', msg: true});
                        default: this.$notify({type: 'error', title: 'ERROR', text: `${res.status}: ${res.responseText}`});
                    }
                })
                .finally((res) => {
                    this.formDisabled = false;
                })
            ;
        },

        checkExists(field) {
            return this.$validator.validate(field)
                .then((res) => {
                    if (!res) throw res;
                    return api.get('/user/check', {[field]: this.userData[field]});
                })
                .then(({data}) => {                    
                    if (data) return this.errors.add({field, rule: 'busy', msg: true});
                })
                // .catch((res) => {
                //     if (!res) return;
                // })
            ;
        },

        removeErrors(selector) {
            console.log('flush', selector, this.errors.collect(selector));
            this.errors.remove(selector);
            // this.errors.erase(selector);
        },
    },
    created() {
        console.log(this.$validator, this.errors);
        
    },
    mounted() {
        // var loginControl = $('#auth input[name="username"]');
        this.$refs.username.focus();
    }
};
