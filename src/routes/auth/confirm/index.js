import './style.styl';

export default {
    template: require('./template.pug')(),
    data() {
        return {
            status: 'pending', // one of null, pending, invalidToken
        }
    },
    methods: {
        confirm() {
            this.status = 'pending';
            let token = this.$route.query.token;
            
            api.get('/auth/confirm/'+token)
                .then(({data}) => {
                    var user = data;
                    this.$store.commit('login', user);

                    this.$notify({type: 'success', title: 'Почта успешно подтверждена.'});
                    this.$router.push({name: 'profile'});
                    this.$notify({type: 'success', title: 'Добро пожаловать!'});
                })
                .catch((e) => {
                    this.status = 'invalidToken';
                })
            ;
        },

    },
    created() {
        this.confirm();
    },
};
