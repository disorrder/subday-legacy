import './style.styl';
import { mapActions } from 'vuex';

export default {
    template: require('./template.pug')(),
    components: {...require("./components")},
    data() {
        return {
            loading: true,
            category: 0,
            previewIcons: {
                roulette: "view-carousel",
            },
            pagination: {
                page: 1,
                limit: 100,
            },
        }
    },
    computed: {
        user() { return this.$store.state.user.user; },
        items() { return this.$store.state.widgets.items; },
        filteredItems() {
            if (this.category === 0) {
                return this.items.filter(item => item.owner === this.user._id);
            } else if (this.category === 1) {
                return [];
                // return this.items;
            } else {
                return this.items.filter(item => item.owner !== this.user._id);
            }
        }
    },
    methods: {
        ...mapActions({
            // createWidget: "widgets/create",
            // getWidgets: "widgets/getList",
        }),

        async addItem() {
            this.$router.push({name: 'widget.create'});
        },

        async getWidgets() {
            let list = await this.$store.state.widgets.getList();
            console.log("widgets", list);
            let userIds = list.map(v => v.owner);
            let {data} = api.get("/users", {params: {ids: userIds}}); // WIP
            console.log("users", data);
    
        },
    },
    async created() {
        this.loading = true;
        await this.getWidgets();
        this.loading = false;
    },
};
