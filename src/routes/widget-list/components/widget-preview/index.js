import "./style.styl";

export default {
    template: require('./template.pug')(),
    props: {
        value: Object, // Widget item
    },
    data() {
        return {

        }
    },
    computed: {
        
    },
    methods: {

    },
    mounted() {

    }
}