import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import store from "../store";

let router = new VueRouter({
    mode: 'history',
    routes: [
        { name: 'main', path: '/', component: require('./main').default },
        { name: 'auth', path: '', component: require('./auth').default,
            children: [
                {name: "auth.login", path: "/login", component: require("./auth/login").default},
            ]
        },
        { name: 'profile', path: '/profile', component: require('./profile').default, meta: {needAuth: true}, },
        
        { name: 'users', path: '/users', component: require('./users').default, meta: {needAuth: true}, },
        // { name: 'user', path: '/user/:id', component: require('./user').default, meta: {needAuth: true}, },
        
        { name: 'widget-list', path: '/widgets', component: require('./widget-list').default, meta: {needAuth: true}, },
        { name: "widget.create", path: "/widget/create", component: require("./widget/create").default, meta: {needAuth: true}, },
        { name: "widget.edit", path: "/widget/:id", component: require("./widget/edit").default, meta: {needAuth: true}, },
        { name: 'wigdet.view', path: '/w/:id', component: require('./w').default }, // widget view for OBS, without auth
        
        { name: 'poll-list', path: '/polls', component: require('./poll-list').default, meta: {needAuth: true}, },
        { name: 'poll', path: '/poll/:id', component: require('./poll').default, meta: {needAuth: true}, 
            children: [
                {name: "poll.edit", path: "edit", component: require("./poll/components/edit").default},
                {name: "poll.view", path: "", component: require("./poll/components/view").default},
            ]
        },
    ]
});
export default router;

//auth guard
router.beforeEach(async (to, from, next) => {
    const needAuth = to.matched.some(record => record.meta.needAuth);
    let isAuthenticated = await store.dispatch("user/isAuthenticated");
    if (needAuth) {
        if (!isAuthenticated) {
            localStorage.redirectTo = location.pathname + location.search;
            return next({name: "auth.login", query: {from: localStorage.redirectTo}});
        }
    }
    if (isAuthenticated && localStorage.redirectTo) {
        next({path: localStorage.redirectTo});
        delete localStorage.redirectTo;
        return;
    }
    return next();
});
