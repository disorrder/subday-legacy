import './style.styl';

export default {
    template: require('./template.pug')(),
    data() {
        return {

        }
    },
    computed: {

    },
    created() {
        this.$router.push({name: "profile"});
    },
    mounted() {

    },
};
