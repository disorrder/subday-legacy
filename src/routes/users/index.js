import './style.styl';
import { mapActions } from 'vuex';

export default {
    template: require('./template.pug')(),
    data() {
        return {
            loading: !true,
            filter: {
                username: "",
                isAdmin: false,
                isStreamer: false,
            },

            headers: [
                {text: "Ava", value: "image", sortable: false, width: 60},
                {text: "Имя", value: "username"},
                {text: "Сабы", value: "twitch.subscribers.score", width: 120},
                {text: "Ссылки", value: "links", sortable: false, width: 100},
                {text: "Стример", value: "access.isStreamer", width: 50},
                {text: "Админ", value: "access.isAdmin", width: 50},
            ],

            tableOptions: {
                itemsPerPage: 10,
                page: 1,
            },
            itemsCount: 100,
            items: [],
        }
    },
    computed: {
        user() { return this.$store.state.user.user; },
        users() { return this.$store.state.users.items; },

        filteredItems() {
            let filter = {...this.filter};
            filter.username = filter.username.toLowerCase();
            return this.users.filter(user => {
                if (filter.username) {
                    let inUser = user.username && user.username.includes(filter.username);
                    let inTwitch = false;
                    if (user.twitch) {
                        inTwitch = user.twitch.display_name.includes(filter.username)
                            || user.twitch.login.includes(filter.username)
                        ;
                    }
                    if (!inUser && !inTwitch) return false;
                }
                return true;
            });
        },
    },
    methods: {
        ...mapActions({
            getUsers: "users/getList",
        }),

        async updateItems() {
            this.loading = true;
            this.items = await this.$store.state.users.getList();
            this.loading = false;
        },

        async __updateItems() {
            let params = {
                ...this.filter,
                after: (this.tableOptions.page-1) * this.tableOptions.itemsPerPage,
                limit: this.tableOptions.itemsPerPage,
            };
            params.username = params.username.toLowerCase()

            await this.$store.state.users.getList({});
        },

        async updateSubs(item) {
            let data = await this.$store.dispatch("user/getTwitchSubsCount", {params: {user_id: item._id}});
            console.log(data);
            item.twitch.subscribers = data;
        },

        userAvatar(user) {
            if (user.twitch) return user.twitch.profile_image_url;
            return "";
        },
    },
    async created() {
        await this.updateItems();
    },
};
