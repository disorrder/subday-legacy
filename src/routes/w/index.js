import './style.styl';
import { mapActions } from 'vuex';

export default {
    template: require('./template.pug')(),
    data() {
        return {
            loading: true,

            size: {
                width: window.innerWidth,
                height: window.innerHeight,
            }
        }
    },
    computed: {
        itemId() { return this.$route.params.id; },
        item() { return this.$store.state.widgets.items.find(v => v._id === this.itemId) },
        
        debugMode() { return this.$route.query.debug; },
        
        user() { return this.$store.state.user.user; },
        owner() { return this.$store.state.users.items.find(v => v._id === this.item.owner) },
        
        widgetSizeStyle() {
            return {width: this.widgetSize-1+'px', height: this.widgetSize+'px'}
        },
    },
    methods: {
        ...mapActions({
            getUser: "users/getById",
            getItem: "widgets/getById",
        }),

        registerWidget() {
            // ws.on("widget.action.run", this.run.bind(this));
            this.item.type.actions.forEach(action => {
                let event = `widget.action.${action.method}`;
                ws.on(`widget.action.${action.method}`, (data) => {
                    this.$refs.widget.$emit(action.method, data);
                });
            })
            ws.on(`widget.action.update`, async () => {
                this.$store.state.widgets.getById(this.itemId);
            });
            ws.emit("widget.register", this.itemId);
            ws.on("hub", (msg) => {
                console.log("hub msg!", msg);
            });
            ws.on("twitch.event", (msg) => {
                console.log("twitch event!", msg);
            });
        },

        onResize() {
            this.size.width = window.innerWidth;
            this.size.height = window.innerHeight;
        }
    },

    async created() {
        this.$store.state.ui.transparent = true;
        this.$store.state.ui.navbar.enabled = false;
        this.$store.state.ui.footer.enabled = false;
        try {
            await this.getItem(this.itemId);
            // await this.getUser(this.item.owner);
        } catch (error) {
            this.$notify({type: "error", text: error})
        }
        this.registerWidget();
    },
    mounted() {
        window.addEventListener("resize", this.onResize.bind(this));
        setTimeout(() => {
            this.$refs.widget.$emit("run");
        }, 2000);
    },
    deleted() {
        this.$store.state.ui.transparent = false;
        this.$store.state.ui.navbar.enabled = true;
        this.$store.state.ui.footer.enabled = true;
    }
};
