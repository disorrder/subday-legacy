import './style.styl';
import { mapActions } from 'vuex';

export default {
    template: require('./template.pug')(),
    data() {
        return {
            loading: true,
            categories: ["my", "subscriber", ""],
            category: 0,
        }
    },
    computed: {
        user() { return this.$store.state.user.user; },
        items() { return this.$store.state.polls.items; },
        filteredItems() {
            if (this.category === 0) return this.items.filter(item => {
                return item.owner === this.user._id;
            });

            if (this.category === 1) return this.items.filter(item => {
                return item.owner !== this.user._id;
            });

            return this.items;
        }
    },
    watch: {
        category() {
            this.updateList();
        }
    },
    methods: {
        ...mapActions({
            createPoll: "polls/create",
            getPolls: "polls/getList",
            getUsers: "users/getList",
        }),

        async addItem() {
            let item = await this.createPoll();
            this.$router.push({name: 'poll.view', params: {id: item._id}});
        },

        async updateList() {
            this.loading = true;
            await this.$store.state.polls.getListByCategory(this.categories[this.category]);
            this.loading = false;
        }
    },
    async created() {
        this.loading = true;
        await this.getUsers();
        let userItems = await this.$store.state.polls.getListByCategory(this.categories[0]);
        // WIP
        // if (!userItems.length) {
        //     this.category = 1;
        // }
        this.loading = false;
    },
};
