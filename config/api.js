const path = require('path');

module.exports = {
    // "host": "https://subday.disordered.ru/api",
    "port": 9010,
    "secretKey": "",
    "db": "",

    "ratelimitEnabled": false,

    "logs": {
        "folder": path.resolve(__dirname, "../.logs"), // "/var/www/website/.logs",
    },
    "upload": {
        "folder": path.resolve(__dirname, "../.public/uploads"), // "/var/www/website/uploads",
    },

    "email": {
        "title": "Disordered Bot",
        "name": "courier",
    },
    "mailgun": {
        "domain": "mail.disordered.ru",
        "apiKey": "",
    },

    "twitch": {
        "url": "https://api.twitch.tv/helix",
        "clientID": "",
        "clientSecret": "",
    },
};
