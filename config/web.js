window.config = {
    api: `/api`,
    ws: `${location.origin.replace("http", "ws")}/ws`,
    twitch: `//api.twitch.tv/helix`,
};
